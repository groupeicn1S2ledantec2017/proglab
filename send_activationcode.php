<?php include('header.php'); ?>

<?php
	header('Content-Type: text/html; charset=utf-8');

	$_SESSION['fbmsg']['emcheckscsmsg']="";
	$_SESSION['fbmsg']['emcheckerrmsg']="";
	$_SESSION['fbmsg']['emchecksolmsg']="";
	if (isset($_POST['sendemail'])) {
		$email = $_POST['email'];

		$query = $conn->prepare("SELECT id,email,activated,blocked FROM users WHERE email = ?");
		$query->execute([$email]);
		$user = $query->fetch();

		if ($user) {
			if (!$user['shutdown']) {
				if (!$user['activated']) {
					$activation_code = random_hexstring(16);
					$_activationCode = new ActivationCode($user['id'], $user['email']);

					$status = $_activationCode->send($activation_code);

					if ($status) {
						$_SESSION['fbmsg']['emcheckscsmsg']="Verification email sent successfully! You can now check your emails to validate your account and then <a href='index.php#login'>log in</a>!";
						$_SESSION['fbmsg']['emchecksolmsg']="Didn't receive an activation email? <a href='email_activationcode.php'>Send again</a>!";
					} else {
						$_SESSION['fbmsg']['emcheckerrmsg']="Verification email not sent! Something went wrong!";
						$_SESSION['fbmsg']['emchecksolmsg']="Please <a href='email_activationcode.php'>try again</a> later!";
					}

				}	else {
					$_SESSION['fbmsg']['emcheckerrmsg']="The account registered with this email address is already activated!";
					$_SESSION['fbmsg']['emchecksolmsg']="You can <a href='index.php#login'>Log in</a>!";
				}
			} else {
				$_SESSION['fbmsg']['emcheckerrmsg']="The account registered with this email address is shut down!";
				$_SESSION['fbmsg']['emchecksolmsg']="We are sorry for that! You can <a href='index.php#signup'>create another account</a>!";
			}
		} else {
			$_SESSION['fbmsg']['emcheckerrmsg']="No user with the email you entered in the database! Please check your informations!";
			$_SESSION['fbmsg']['emchecksolmsg']="Not registered yet? <a href='index.php#signup'>Create an account</a>!";
		}

		header("Location: email_activationcode.php");
		exit;
	}
?>

<?php include('footer.php'); ?>
