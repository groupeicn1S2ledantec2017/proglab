<?php include('header.php'); ?>

<?php
if (isset($_GET['user'])) {
	$username = $_GET['user'];

	if (!(!preg_match('/[^A-Za-z0-9_-]/', substr($username , 1)) && $username[0]=='@')) {
		echo '
			<div class="info">
				<h1>Invalid username.</h1><br/>
				<h2>You will be redirected on your home page.</h2>
			</div>
			<meta http-equiv="refresh" content="2; url=home.php" />';
		include ("footer.php");
		die();
	}
}
else {
	$username = $_SESSION['username'];
}
$username = htmlspecialchars($username);

$u_query = $conn->prepare("SELECT * FROM users WHERE username=?");
$u_query->execute([$username]);
$user = $u_query->fetch();

if ($user) {
	if ($user['activated']) {
		if (!$user['shutdown']) {
			if (isset($_SESSION['username'])) {
				if (!strpos($user['blocked'], $_SESSION['username'])===false) {
					echo '
						<div class="info">
							<h1>Profile unavailable !</h1>
							<h3>You were blocked by this user.</h3><br/>
							<h2>You will be redirected on your home page.</h2>
						</div>
						<meta http-equiv="refresh" content="3; url=home.php" />';
					include ("footer.php");
					die();
				}
			}
			$_SESSION['profile']['username']=$user["username"];
			$_SESSION['profile']['firstname']=$user["firstname"];
			$_SESSION['profile']['lastname']=$user["lastname"];
			$_SESSION['profile']['pseudo']=$user["pseudo"];
			$_SESSION['profile']['email']=$user["email"];
			$_SESSION['profile']['biography']=$user["biography"];
			$_SESSION['profile']['gender']=$user["gender"];
			$_SESSION['profile']['birthday']=$user["birthday"];
			$_SESSION['profile']['last_activity']=$user["last_activity"];
			$_SESSION['profile']['friends']=$user["friends"];
			$_SESSION['profile']['ffriends']=$user["ffriends"];
			$_SESSION['profile']['profile_pic']=$user["profile_pic"];
			$_SESSION['profile']['followings']=$user["followings"];
			$_SESSION['profile']['followers']=$user["followers"];
			$_SESSION['profile']['languages']=$user["languages"];
			$_SESSION['profile']['prog_languages']=$user["prog_languages"];
			$_SESSION['profile']['prog_paradigms']=$user["prog_paradigms"];
			$_SESSION['profile']['interests']=$user["interests"];
		} else {
			echo '
				<div class="info">
					<h1>Profile unavailable !</h1>
					<h3>This account is shut down.</h3><br/>
					<h2>You will be redirected on your home page.</h2>
				</div>
				<meta http-equiv="refresh" content="2; url=home.php" />';
			include ("footer.php");
			die();
		}
	} else {
		echo '
			<div class="info">
				<h1>Profile unavailable !</h1>
				<h3>This account is unactivated.</h3><br/>
				<h2>You will be redirected on your home page.</h2>
			</div>
			<meta http-equiv="refresh" content="2; url=home.php" />';
		include ("footer.php");
		die();
	}
} else {
	echo '
		<div class="info">
			<h1>User with username '.$username.' not found.</h1>
			<h3>The account may have been deleted.</h3><br/>
			<h2>You will be redirected on your home page.</h2>
		</div>
		<meta http-equiv="refresh" content="2; url=home.php" />';
	include ("footer.php");
	die();
}
?>

<div>
	<div class="box">
	<?php
	$date = new DateTime(date('Y-m-d H:i:s'));

	if (isset($_SESSION["username"])) if ($_SESSION['profile']['username'] != $_SESSION['username']) $_followform = new FollowForm;

	if (isset($_SESSION["username"])) {
		if ($_SESSION["username"]!=$_SESSION['profile']['username']) echo '<h1>'.$_SESSION['profile']['username'].'\'s profile</h1>';
		else echo '<h1>Your profile</h1>';
	} else echo '<h1>'.$_SESSION['profile']['username'].'\'s profile</h1>';

	$lastact = new DateTime($_SESSION['profile']['last_activity']);
	$lactdiff = $date->diff($lastact);
	if ((int)$lactdiff->format('%y') > 0) echo '<div class="pwrap"><h3 class="fbblockdiv csub">Offline</h3><h4>Last activity : '.$lastact->format('l, F j, Y - g:i A').$lactdiff->format(' (%Y years ago)').'</h4></div>';
	else if ((int)$lactdiff->format('%m') > 0) echo '<div class="pwrap"><h3 class="fbblockdiv csub">Offline</h3><h4>Last activity : '.$lastact->format('l, F j, Y - g:i A').$lactdiff->format(' (%m month ago)').'</h4></div>';
	else if ((int)$lactdiff->format('%d') > 0) echo '<div class="pwrap"><h3 class="fbblockdiv csub">Offline</h3><h4>Last activity : '.$lastact->format('l, F j, Y - g:i A').$lactdiff->format(' (%d days ago)').'</h4></div>';
	else if ((int)$lactdiff->format('%h') > 0) echo '<div class="pwrap"><h3 class="fbblockdiv csub">Offline</h3><h4>Last activity : '.$lastact->format('l, F j, Y - g:i A').$lactdiff->format(' (%h hours ago)').'</h4></div>';
	else if ((int)$lactdiff->format('%i') > 10)  echo '<div class="pwrap"><h3 class="fbblockdiv csub">Offline</h3><h4>Last activity : '.$lastact->format('l, F j, Y - g:i A').$lactdiff->format(' (%i minutes ago)').'</h4></div>';
	else if ((int)$lactdiff->format('%i') < 10) echo '<div class="pwrap"><h3 class="fbblockdiv asub">Online</h3><br/><br/></div>';
	else echo '<div class="pwrap"><h4>Unknown last activity</h4></div>';

	if (isset($_SESSION["username"])) if ($_SESSION['profile']['username'] != $_SESSION['username']) $_friendrequestform = new FriendRequestForm;

	if ($_SESSION['profile']['profile_pic']!="") echo "<img src='".$_SESSION['profile']['profile_pic']."' class='bpic' />";

	echo "<br/><h2>".$_SESSION['profile']['firstname']." ".$_SESSION['profile']['lastname']." (".$_SESSION['profile']['pseudo'].")</h2><br/>";

	$bday = new DateTime($_SESSION['profile']['birthday']);
	$age = $date->diff($bday);
	echo "<h3>".ucfirst($_SESSION['profile']['gender']).", born on ".$bday->format('F j, Y')." (aged ".$age->y." years, ".$age->m." months, ".$age->d." day).</h3>";
	echo "<h3><u>Email :</u> <i><a href=mailto:".$_SESSION['profile']['email'].">".$_SESSION['profile']['email']."</a></h3></i>";

	if ($_SESSION['profile']['biography']!="") echo "<h3><u>Biography :</u> <i>".$_SESSION['profile']['biography']."</h3></i>";

	if ($_SESSION['profile']['languages']!="") {
		echo "<h3><u>Spoken Languages :</u> <i>";
		$str = "";
		foreach (explode(",",$_SESSION['profile']['languages']) as $l) {
			if ($l) {
				$query = $conn->query("SELECT language FROM languages WHERE id='$l'");
				$uresult = $query->fetch();
				$str .= $uresult['language'].", ";
			}
		}
		echo substr($str,0,strlen($str)-2);
		echo "</i></h3>";
	}
	if ($_SESSION['profile']['prog_languages']!="") {
		echo "<h3><u>Favourite Programming Languages :</u> <i>";
		$str = "";
		foreach (explode(",",$_SESSION['profile']['prog_languages']) as $pl) {
			if ($pl) {
				$query = $conn->query("SELECT prog_language FROM prog_languages WHERE id='$pl'");
				$uresult = $query->fetch();
				$str .= $uresult['prog_language'].", ";
			}
		}
		echo substr($str,0,strlen($str)-2);
		echo "</i></h3>";
	}
	if ($_SESSION['profile']['prog_paradigms']!="") {
		echo "<h3><u>Favourite Programming Paradigms :</u> <i>";
		$str = "";
		foreach (explode(",",$_SESSION['profile']['prog_paradigms']) as $pp) {
			if ($pp) {
				$query = $conn->query("SELECT prog_paradigm FROM prog_paradigms WHERE id='$pp'");
				$uresult = $query->fetch();
				$str .= $uresult['prog_paradigm'].", ";
			}
		}
		echo substr($str,0,strlen($str)-2);
		echo "</i></h3>";
	}
	if ($_SESSION['profile']['interests']!="") {
		echo "<h3><u>Interests :</u> <i>";
		$str = "";
		foreach (explode(",",$_SESSION['profile']['interests']) as $i) {
			if ($i) {
				$query = $conn->query("SELECT interest FROM interests WHERE id='$i'");
				$uresult = $query->fetch();
				$str .= $uresult['interest'].", ";
			}
		}
		echo substr($str,0,strlen($str)-2);
		echo "</i></h3>";
	}

	echo '<h3 id="hfriends">Friends ('.(count(explode(',',$_SESSION['profile']['friends']))-1).')</h3>';
	echo '<div id="friends" class="pwrap">';
	foreach (explode(',',$_SESSION['profile']['friends']) as $friend) {
		if ($friend) {
			if (isset($_SESSION["username"])) {
				if ($friend != $_SESSION['username']) echo '<a href="profile.php?user='.$friend.'" class="fbblockdiv dsubh">'.$friend.'</a>';
				else  echo '<a id="youfriend" class="fbblockdiv sub">You</a>';
			} else echo '<a href="profile.php?user='.$friend.'" class="fbblockdiv dsubh">'.$friend.'</a>';
		}
	}
	echo '</div>';
	echo '<h3 id="followings">Followings ('.(count(explode(',',$_SESSION['profile']['followings']))-1).')</h3>';
	echo '<div id="followings" class="pwrap">';
	foreach (explode(',',$_SESSION['profile']['followings']) as $following) {
		if ($following) {
			if (isset($_SESSION["username"])) {
				if ($following != $_SESSION['username']) echo '<a href="profile.php?user='.$following.'" class="fbblockdiv dsubh">'.$following.'</a>';
				else  echo '<a class="fbblockdiv sub">You</a>';
			} else echo '<a href="profile.php?user='.$following.'" class="fbblockdiv dsubh">'.$following.'</a>';
		}
	}
	echo '</div>';
	echo '<h3 id="hfollowers">Followers ('.(count(explode(',',$_SESSION['profile']['followers']))-1).')</h3>';
	echo '<div id="followers" class="pwrap">';
	foreach (explode(',',$_SESSION['profile']['followers']) as $follower) {
		if ($follower) {
			if (isset($_SESSION["username"])) {
				if ($follower != $_SESSION['username']) echo '<a href="profile.php?user='.$follower.'" class="fbblockdiv dsubh">'.$follower.'</a>';
				else  echo '<a id="youfollow" class="fbblockdiv sub">You</a>';
			} else echo '<a href="profile.php?user='.$follower.'" class="fbblockdiv dsubh">'.$follower.'</a>';
		}
	}
	echo '</div>';
	?>
	</div>

	<?php if (isset($_SESSION["username"])) $_postform = new PostForm; ?>
</div>

<div>
	<?php
	$i = 10;
	$i++;
	if (isset($_SESSION['username']) && (strpos($_SESSION['profile']['friends'],$_SESSION['username'].',')===true || $_SESSION['username']==$_SESSION['profile']['username'])) $access = 'friends';
	else if (isset($_SESSION['username']) && strpos($_SESSION['profile']['ffriends'],$_SESSION['username'].',')===true) $access = 'ffriends';
	else $access = 'public';

	$p_query = $conn->prepare("
	    SELECT id,date_added
	    FROM
	        posts
	    WHERE
	        ((posted_to=:pun OR posted_by=:pun) AND visibility=:a) OR ((posted_to=:pun AND posted_by=:un) OR (posted_to=:un AND posted_by=:pun))
      ORDER BY date_added DESC -- LIMIT $i");
	$p_query->execute(['pun'=>$_SESSION['profile']['username'],'un'=>$_SESSION['username'],'a'=>$access]);
	$i--;
	if ($post = $p_query->fetch()) {
		echo '<div id="posts"><hr/><h3>Posts</h3><hr/>';
		do {
			$post = new PostItem($post['id'], 'posts');
			$post->display();
		} while (/*--$i > 0 &&*/ ($post = $p_query->fetch()));
			echo '</div>';
	} else {
		echo '<div id="posts"><hr/><h3>No posts to show</h3><hr/></div>';
	}
	//if ($p_result->fetch_assoc()) echo '<div class="bblockdiv dsubh">Load More</div>';
	?>
</div>

<?php unset($_SESSION['profile']); ?>

<?php include('footer.php'); ?>
