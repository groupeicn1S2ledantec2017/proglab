<?php include('header.php'); ?>

<form id="search" class="box" action="" method="GET">
	<h2>Search</h2>
	<hr/>
	<select name="searchtype" onchange="searchtypes();">
		<option value="user">User</option>
		<option value="friend">Friend</option>
		<option value="subject">Subject</option>
	</select>
	<?php if (isset($_GET['searchtype'])) echo '<script type="text/javascript">$("#search select").val("'.$_GET['searchtype'].'"); if (!$("#search select").val()) $("#search select").val("user"); searchtypes();</script>'; ?>
	<div class="searchopt" id="usersearch">
		<input type="search" name="search" placeholder="Search..." value="<?php echo isset($_GET['search']) ? $_GET['search'] : '' ?>" required />
		<h3>Search a user by username, first name, last name, pseudo or email.</h3>
	</div>
	<div class="searchopt" id="friendsearch">
		<table class="formtable searchtable">
			<tr>
				<th>Age</th>
				<td>
					<strong>Min : </strong>
					<input id="amin" type="number" onchange="respectRange('amin', 'amax');" name="amin" min="0" max="120" value="<?php echo ((isset($_GET['amin']) && is_numeric(htmlentities($_GET['amin']))) ? htmlentities($_GET['amin']) : '0'); ?>"/>
					<strong> Max : </strong>
					<input id="amax" type="number" onchange="respectRange('amin', 'amax', false);" name="amax" min="0" max="120" value="<?php echo ((isset($_GET['amax']) && is_numeric(htmlentities($_GET['amax']))) ? htmlentities($_GET['amax']) : '120'); ?>"/></td>
			</tr>
			<tr>
				<th>Gender</th>
				<td>
					<table class="formtable">
						<tr>
							<td><input id="allradio" type="radio" name="gender" value="any" checked /><label for="anyradio"> Any </label></td>
							<td><input id="maleradio" type="radio" name="gender" value="male" /><label for="maleradio"> Male </label></td>
							<td><input id="femaleradio" type="radio" name="gender" value="female"/><label for="femaleradio"> Female </label></td>
							<td><input id="otherradio" type="radio" name="gender" value="other"  /><label for="otherradio"> Other </label></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<th>Spoken Languages</th>
				<td>
					<dl id="spoklang" class="dropdown">
						<input type='text' name='lang' class="formMultiSel" hidden />
						<input type='text' class="multiSel readonly" readonly disabled />
						<div class="nothSel">Nothing selected</div>
				    <dt id="spoklangt" onclick="dropdown('spoklang');">
				      <div>Select</div>
				    </dt>
				    <dd>
			        <div class="mutliSelect">
		            <ul>
									<?php
									$query = $conn->query("SELECT id,language FROM languages") or die($conn->error);
									while ($result = $query->fetch()) echo '<li><input type="checkbox" value="'.$result['id'].'" />'.$result['language'].'</li>';
									?>
		            </ul>
			        </div>
				    </dd>
					</dl>
				</td>
			</tr>
			<tr>
				<th>Programming Languages</th>
				<td>
					<dl id="proglang" class="dropdown">
						<input type='text' name='proglang' class="formMultiSel" hidden />
						<input type='text' class="multiSel readonly" readonly disabled />
						<div class="nothSel">Nothing selected</div>
				    <dt onclick="dropdown('proglang');">
				      <div>Select</div>
				    </dt>
				    <dd>
			        <div class="mutliSelect">
		            <ul>
									<?php
									$query = $conn->query("SELECT id,prog_language FROM prog_languages");
									while ($result = $query->fetch()) echo '<li><input type="checkbox" value="'.$result['id'].'" />'.$result['prog_language'].'</li>';
									?>
								</ul>
			        </div>
				    </dd>
					</dl>
				</td>
			</tr>
			<tr>
				<th>Programming Paradigms</th>
				<td>
					<dl id="progpara" class="dropdown">
						<input type='text' name='progpara' class="formMultiSel" hidden />
						<input type='text' class="multiSel readonly" readonly disabled />
						<div class="nothSel">Nothing selected</div>
				    <dt onclick="dropdown('progpara');">
				      <div>Select</div>
				    </dt>
				    <dd>
			        <div class="mutliSelect">
		            <ul>
									<?php
									$query = $conn->query("SELECT id,prog_paradigm FROM prog_paradigms");
									while ($result = $query->fetch()) echo '<li><input type="checkbox" value="'.$result['id'].'" />'.$result['prog_paradigm'].'</li>';
									?>
								</ul>
			        </div>
				    </dd>
					</dl>
				</td>
			</tr>
			<tr>
				<th>Interests</th>
				<td>
					<dl id="interests" class="dropdown">
						<input type='text' name='interests' class="formMultiSel" hidden />
						<input type='text' class="multiSel readonly" readonly disabled />
						<div class="nothSel">Nothing selected</div>
				    <dt onclick="dropdown('interests');">
				      <div>Select</div>
				    </dt>
				    <dd>
			        <div class="mutliSelect">
		            <ul>
									<?php
									$query = $conn->query("SELECT id,interest FROM interests");
									while ($result = $query->fetch()) echo '<li><input type="checkbox" value="'.$result['id'].'" />'.$result['interest'].'</li>';
									?>
		            </ul>
			        </div>
				    </dd>
					</dl>
				</td>
			</tr>
		</table>
	</div>
	<div class="searchopt" id="subjectsearch">
		<input type="search" name="search" placeholder="Search..." value="<?php echo isset($_GET['search']) ? $_GET['search'] : '' ?>" required />
		<table class="formtable">
			<tr>
				<th>Sort</th>
				<td><input id="relsearch" type="radio" name="searchmode" value="relevance" checked /><label for="relsearch"> Relevance </label></td>
				<td><input id="datsearch" type="radio" name="searchmode" value="lastadded" /><label for="datsearch"> Last added </label></td>
			</tr>
		</table>
		<br/>
		<h3>Use @ to identify a user and # to identify a subject</h3>
		<h4 style="text-align:center;">You can use boolean search : more info <a href="https://dev.mysql.com/doc/refman/5.5/en/fulltext-boolean.html">here</a></h4>
		<h4 style="text-align:center;">Special characters should be placed between doublequotes to be searched in posts.</h4>
		<h4 style="text-align:center;">A leading plus sign (+) indicates that this word must be present in each post that is returned.</h4>
		<h4 style="text-align:center;">A leading minus sign (-) indicates that this word must not be present in any of the posts that are returned.</h4>
		<h4 style="text-align:center;">By default (when neither + nor - is specified) the word is optional, but the posts that contain it are rated higher.</h4>
		<h4 style="text-align:center;">A leading greater than (&gt;) or lower than (&lt;) sign is used to change a word's contribution to the relevance value that is assigned to a post. The &gt; operator increases the contribution and the &lt; operator decreases it.</h4>
		<h4 style="text-align:center;">Parentheses group words into subexpressions. Parenthesized groups can be nested.</h4>
		<h4 style="text-align:center;">A leading tilde (~) acts as a negation operator, causing the word's contribution to the post's relevance to be negative. This is useful for marking “noise” words. A row containing such a word is rated lower than others, but is not excluded altogether, as it would be with the - operator.</h4>
		<h4 style="text-align:center;">The asterisk (*) serves as the truncation (or wildcard) operator. Unlike the other operators, it should be appended to the word to be affected. Words match if they begin with the word preceding the * operator.</h4>
		<h4 style="text-align:center;">A phrase that is enclosed within double quote (") characters matches only rows that contain the phrase literally, as it was typed. Nonword characters need not be matched exactly: Phrase searching requires only that matches contain exactly the same words as the phrase and in the same order.</h4>
		<h4 style="text-align:center;">Each word with a length lower than 3 is not accounted.</h4>
	</div>
	<input type="submit" value="Search" />
</form>

<script type="text/javascript">
	$(window).click(function(event) { if(!$(event.target).parents().hasClass("dropdown")) $('.dropdown .dactive').parent().each(function() {dropdown($(this).attr('id'));})});
	$(document).ready(function() { dropdown(); searchtypes(); respectRange('amin', 'amax');});
</script>

<br/><br/>

<?php
if(isset($_GET['searchtype'])) {
	if ($_GET['searchtype']=='user') {
		if(isset($_GET['search'])) {
			$search = $_GET['search'];

			$results = array();
			$results_arr = array();
			$results_rdata = array();
			$esearchs = explode(" ", $search);
			foreach ($esearchs as $i => $esearch) {
				array_push($results_arr, array());
				$rsearch = '%'.$esearch.'%';
				$query = $conn->prepare('SELECT id,username,firstname,lastname,pseudo,email FROM users WHERE username LIKE :rs OR firstname LIKE :rs OR lastname LIKE :rs OR pseudo LIKE :rs OR email LIKE :rs');
				$query->execute(['rs'=>$rsearch]);

				while ($rresult = $query->fetch()) {
					array_push($results_arr[$i],$rresult['id']);
					array_push($results_rdata,$rresult);
				}
			}
			foreach ($results_arr as $rresults) {
				foreach ($rresults as $rresult) {
					$test = true;
					foreach ($results_arr as $results_check) {
						if (!in_array($rresult, $results_check)) $test = false;
					}
					if ($test) array_push($results,$rresult);
				}
			}
			$results = array_unique($results);
			$results_data = array();

			foreach ($results_rdata as $result_rdata) {
				$res = array_search($result_rdata['id'], $results);
				if ($res!==false) {
					unset($results[$res]);
					array_push($results_data,$result_rdata);
				}
			}
			if (count($results_data)>0) {
				echo '
					<h2>Results for the search of user : '.htmlentities($search).'</h2>
					<table class="resulttable"><tr><th>Username</th><th>Firstname</th><th>Lastname</th><th>Pseudo</th><th>Email</th></tr>';

				foreach ($results_data as $result_data) {
					echo '<tr><td><a href="profile.php?user='.$result_data['username'].'">'.$result_data['username'].'</a></td><td>'.$result_data['firstname'].'</td><td>'.$result_data['lastname'].'</td><td>'.$result_data['pseudo'].'</td><td>'.$result_data['email'].'</td></tr>';
				}
				echo '</table>';
			} else {
				echo '<h2>No result for the search of user : '.htmlentities($search).'</h2>';
			}
		}
	} else if ($_GET['searchtype']=='friend') {
		if (isset($_GET['amin']) && isset($_GET['amax']) && isset($_GET['gender'])) {
			$amin = htmlentities(isset($_GET['amin']) ? $_GET['amin'] : '0');
			$ramin=$amin;
			$amax = htmlentities(isset($_GET['amax']) ? $_GET['amax'] : '120');
			$ramax=$amax;
			if (!is_numeric($amin) || !is_numeric($amax) || $amin<0 || $amax>120) {
				echo '<h2>Invalid age values : <u>'.$amin.'</u> and/or <u>'.$amax.'</u></h2>';
				include('footer.php');
				die();
			}
			$amax++;
			$amin = strtotime("-".$amin." years", time());
			$amax = strtotime("-".$amax." years", time());
			$dmin = date('Y-m-d', $amax);
			$dmax = date('Y-m-d', $amin);

			$query = $conn->prepare('SELECT id FROM users WHERE (birthday BETWEEN ? AND ?)');
			$query->execute([$dmin, $dmax]);

			$search_results = array();
			while ($result = $query->fetch()) array_push($search_results, $result['id']);

			$gender = htmlentities(isset($_GET['gender']) ? $_GET['gender'] : 'any');
			if (!in_array($gender, array('any', 'male', 'female', 'other'))) {
				echo '<h2>Invalid gender value : <u>'.$gender.'</u></h2>';
				include('footer.php');
				die();
			}
			if ($gender!="any") {
				$query = $conn->prepare('SELECT id FROM users WHERE (gender=?)');
				$query->execute([$gender]);
				$oresults = array();
				while ($result = $query->fetch()) array_push($oresults, $result['id']);
				$search_results = array_intersect($search_results,$oresults);
			}

			$lang = htmlentities(isset($_GET['lang']) ? $_GET['lang'] : '');
			if (!is_numeric(str_replace(" ","",$lang)) && $lang!="") {
				echo '<h2>Invalid spoken languages values</h2>';
				include('footer.php');
				die();
			}
			$lang_str = "";
			if ($lang!="") {
				foreach (explode(" ",$lang) as $l) {
					$query = $conn->prepare("SELECT language FROM languages WHERE id=?");
					$query->execute([$l]);
					$result = $query->fetch();
					$lang_str .= '<span style="color:black;font-style: italic;">'.$result['language']."</span>, ";

					$l = '%'.$l.',%';
					$query = $conn->prepare("SELECT id FROM users WHERE languages LIKE ?");
					$query->execute([$l]);
					$oresults = array();
					while ($result = $query->fetch()) array_push($oresults, $result['id']);
					$search_results = array_intersect($search_results,$oresults);
				}
			}
			$proglang = htmlentities(isset($_GET['proglang']) ? $_GET['proglang'] : '');
			if (!is_numeric(str_replace(" ","",$proglang)) && $proglang!="") {
				echo '<h2>Invalid programming languages values</h2>';
				include('footer.php');
				die();
			}
			$proglang_str = "";
			if ($proglang!="") {
				foreach (explode(" ",$proglang) as $pl) {
					$query = $conn->prepare("SELECT prog_language FROM prog_languages WHERE id=?");
					$query->execute([$pl]);
					$result = $query->fetch();
					$proglang_str .= '<span style="color:black;font-style: italic;">'.$result['prog_language']."</span>, ";

					$pl = '%'.$pl.',%';
					$query = $conn->prepare("SELECT id FROM users WHERE prog_languages LIKE ?");
					$query->execute([$pl]);
					$oresults = array();
					while ($result = $query->fetch()) array_push($oresults, $result['id']);
					$search_results = array_intersect($search_results,$oresults);
				}
			}
			$progpara = htmlentities(isset($_GET['progpara']) ? $_GET['progpara'] : '');
			if (!is_numeric(str_replace(" ","",$progpara)) && $progpara!="") {
				echo '<h2>Invalid programming paradigms values</h2>';
				include('footer.php');
				die();
			}
			$progpara_str = "";
			if ($progpara!="") {
				foreach (explode(" ",$progpara) as $pp) {
					$query = $conn->prepare("SELECT prog_paradigm FROM prog_paradigms WHERE id=?");
					$query->execute([$pp]);
					$result = $query->fetch();
					$progpara_str .= '<span style="color:black;font-style: italic;">'.$result['prog_paradigm']."</span>, ";

					$pp = '%'.$pp.',%';
					$query = $conn->prepare("SELECT id FROM users WHERE prog_paradigms LIKE ?");
					$query->execute([$pp]);
					$oresults = array();
					while ($result = $query->fetch()) array_push($oresults, $result['id']);
					$search_results = array_intersect($search_results,$oresults);
				}
			}
			$interests = htmlentities((isset($_GET['interests']) ? $_GET['interests'] : ''));
			if (!is_numeric(str_replace(" ","",$interests)) && $interests!="") {
				echo '<h2>Invalid interests values</h2>';
				include('footer.php');
				die();
			}
			$interests_str = "";
			if ($interests!="") {
				foreach (explode(" ",$interests) as $i) {
					$query = $conn->prepare("SELECT interest FROM interests WHERE id=?");
					$query->execute([$i]);
					$result = $query->fetch();
					$interests_str .= '<span style="color:black;font-style: italic;">'.$result['interest']."</span>, ";

					$i = '%'.$i.',%';
					$query = $conn->prepare("SELECT id FROM users WHERE interests LIKE ?");
					$query->execute([$i]);
					$oresults = array();
					while ($result = $query->fetch()) array_push($oresults, $result['id']);
					$search_results = array_intersect($search_results,$oresults);
				}
			}

			$searchparams = '<h3><u>Age :</u> between <span style="color:black;font-style: italic;">'.$ramin.'</span> and <span style="color:black;font-style: italic;">'.$ramax.'</span>,</h3><h3> <u>Gender :</u> <span style="color:black;font-style: italic;">'.$gender.($lang_str ? '</span>,</h3><h3> <u>Spoken languages :</u> '.$lang_str : '').($proglang_str ? '</h3><h3> <u>Programming languages :</u> '.$proglang_str : '').($progpara_str ? '</h3><h3> <u>Programming paradigms :</u> '.$progpara_str : '').($interests_str ? '</h3><h3> <u>Interests :</u> '.$interests_str : '').'</h3>';

			if (count($search_results)) {
				echo '
					<h2>Results for the search of friends with parameters : </h2>'.$searchparams.'
					<table class="resulttable"><tr><th>Username</th><th>Gender</th><th>Spoken Languages</th><th>Programming Languages</th><th>Programming Paradigms</th><th>Interests</th><th>Birthday</th></tr>';
				foreach ($search_results as $id) {
					$query = $conn->query("SELECT username,gender,languages,prog_languages,prog_paradigms,interests,birthday FROM users WHERE id='$id'");
					$result = $query->fetch();
					$ulang_str='';
					foreach (explode(",",$result['languages']) as $l) {
						if ($l) {
							$query = $conn->query("SELECT language FROM languages WHERE id='$l'");
							$uresult = $query->fetch();
							$ulang_str .= $uresult['language'].", ";
						}
					}
					$ulang_str = substr($ulang_str, 0, strlen($ulang_str)-2);
					$uproglang_str='';
					foreach (explode(",",$result['prog_languages']) as $pl) {
						if ($pl) {
							$query = $conn->query("SELECT prog_language FROM prog_languages WHERE id='$pl'");
							$uresult = $query->fetch();
							$uproglang_str .= $uresult['prog_language'].", ";
						}
					}
					$uproglang_str = substr($uproglang_str, 0, strlen($uproglang_str)-2);
					$uprogpara_str='';
					foreach (explode(",",$result['prog_paradigms']) as $pp) {
						if ($pp) {
							$query = $conn->query("SELECT prog_paradigm FROM prog_paradigms WHERE id='$pp'");
							$uresult = $query->fetch();
							$uprogpara_str .= $uresult['prog_paradigm'].", ";
						}
					}
					$uprogpara_str = substr($uprogpara_str, 0, strlen($uprogpara_str)-2);
					$uinterests_str='';
					foreach (explode(",",$result['interests']) as $i) {
						if ($i) {
							$query = $conn->query("SELECT interest FROM interests WHERE id='$i'");
							$uresult = $query->fetch();
							$uinterests_str .= $uresult['interest'].", ";
						}
					}
					$uinterests_str = substr($uinterests_str, 0, strlen($uinterests_str)-2);
					echo '<tr><td><a href="profile.php?user='.$result['username'].'">'.$result['username'].'</a></td><td>'.$result['gender'].'</td><td>'.$ulang_str.'</td><td>'.$uproglang_str.'</td><td>'.$uprogpara_str.'</td><td>'.$uinterests_str.'</td><td>'.$result['birthday'].'</td></tr>';
				}
				echo '</table>';
			}
			else {
				echo '<h2>No result for the search of friends with parameters : </h2>'.$searchparams;
			}
		}
	} else if ($_GET['searchtype']=='subject') {
		if (isset($_GET['search'])) {
			$search = $_GET['search'];

			if (!isset($_GET['searchmode'])) $_GET['searchmode']='relevance';
			if (isset($_SESSION['username'])) {
				switch ($_GET['searchmode']) {
					case 'lastadded':
						$query = $conn->prepare("
							SELECT
								posts.id,posts.date_added, MATCH(posts.body) AGAINST(:se IN BOOLEAN MODE) AS relevance
							FROM
								posts INNER JOIN users ON ((users.followings LIKE CONCAT('%',posts.posted_by,',%') OR users.followings LIKE CONCAT('%',posts.posted_to,',%')) OR (users.username=posts.posted_by OR users.username=posts.posted_to))
							WHERE
								(
									(users.username=:un AND posts.posted_by=:un)
									OR
									(users.username=:un AND posts.posted_to=:un)
									OR
									(users.username=:un AND posts.visibility='public')
									OR
									(users.username=:un AND posts.visibility='ffriends' AND (users.ffriends LIKE CONCAT('%',posts.posted_by,',%') OR users.ffriends LIKE CONCAT('%',posts.posted_to,',%')))
									OR
									(users.username=:un AND posts.visibility='friends' AND (users.friends LIKE CONCAT('%',posts.posted_by,',%') OR users.friends LIKE CONCAT('%',posts.posted_to,',%')))
								)
								AND
								MATCH(posts.body) AGAINST (:se IN BOOLEAN MODE) > 0
							ORDER BY date_added DESC");
							$smode = "last added";
						break;
					case 'relevance':
						$query = $conn->prepare("
							SELECT
								posts.id,posts.date_added, MATCH(posts.body) AGAINST(:se IN BOOLEAN MODE) AS relevance
							FROM
								posts INNER JOIN users ON ((users.followings LIKE CONCAT('%',posts.posted_by,',%') OR users.followings LIKE CONCAT('%',posts.posted_to,',%')) OR (users.username=posts.posted_by OR users.username=posts.posted_to))
							WHERE
								(
									(users.username=:un AND posts.posted_by=:un)
									OR
									(users.username=:un AND posts.posted_to=:un)
									OR
									(users.username=:un AND posts.visibility='public')
									OR
									(users.username=:un AND posts.visibility='ffriends' AND (users.ffriends LIKE CONCAT('%',posts.posted_by,',%') OR users.ffriends LIKE CONCAT('%',posts.posted_to,',%')))
									OR
									(users.username=:un AND posts.visibility='friends' AND (users.friends LIKE CONCAT('%',posts.posted_by,',%') OR users.friends LIKE CONCAT('%',posts.posted_to,',%')))
								)
								AND
								MATCH(posts.body) AGAINST (:se IN BOOLEAN MODE) > 0
							ORDER BY relevance DESC, date_added DESC");
							$smode = "relevance";
						break;
				}
				$query->execute(['se'=>$search, 'un'=>$_SESSION['username']]);
			} else {
				switch ($_GET['searchmode']) {
					case 'lastadded':
						$query = $conn->prepare("
							SELECT
								'text' AS type,posts.id,posts.date_added, MATCH(posts.body) AGAINST(:se IN BOOLEAN MODE) AS relevance
							FROM
								posts
							WHERE
								posts.visibility='public'
								AND
								MATCH(posts.body) AGAINST (:se IN BOOLEAN MODE) > 0
							ORDER BY date_added DESC");
							$smode = "last added";
						break;
					case 'relevance':
						$query = $conn->prepare("
							SELECT
								'text' AS type,posts.id,posts.date_added, MATCH(posts.body) AGAINST(:se IN BOOLEAN MODE) AS relevance
							FROM
								posts
							WHERE
								posts.visibility='public'
								AND
								MATCH(posts.body) AGAINST (:se IN BOOLEAN MODE) > 0
							ORDER BY relevance DESC, date_added DESC");
							$smode = "relevance";
						break;
				}
				$query->execute(['se'=>$search]);
			}
			if (!$query) {
				echo '<h2>Search not allowed <span style="color:black;font-style: italic;">'.htmlentities($search).'</span></h2><h3>Use doublequotes to escape words containing special chararcters</h3>';
			} else {
				if ($result = $query->fetch()) {
					echo '<h2>Results for the boolean search of subject <span style="color:black;font-style: italic;">'.htmlentities($search).'</span> sorted by '.$smode.'.</h2>';
					do {
						echo '<h5>Relevance : '.$result['relevance'].'</h5>';
						$post = new PostItem($result['id'], 'posts');
						$post->display();
					} while ($result = $query->fetch());
				} else {
					echo '<h2>No result for the search of subject <span style="color:black;font-style: italic;">'.htmlentities($search).'</span></h2>';
				}
			}
		}
	} else {
		echo '<h2>Invalid search type : <span style="color:black;"><u>'.htmlentities($_GET['searchtype']).'</u></span></h2>';
		include('footer.php');
		die();
	}
}
?>

<?php include('footer.php'); ?>
