<?php include("header.php"); ?>

<div class="info">

<?php
if (isset($_GET['user']) && isset($_GET['code']) ) {
	$u = $_GET['user'];
	$code = strtolower($_GET['code']);

	$query = $conn->prepare("SELECT username,email,activated,blocked,shutdown FROM users WHERE id = ?");
	$query->execute([$u]);
	$user = $query->fetch();

	if ($user) {
		if (!$user['shutdown']) {
			if (!$user['activated']) {
				$select_query = $conn->query("SELECT activation_code FROM account_activation WHERE username='$user[username]'");
				$select_result = $select_query->fetch();

				if (isset($select_result['activation_code'])) {
					$_activationCode = new ActivationCode($user['username'], $user['email']);

					if ($_activationCode->check($code)) {
						$activated=1;
						$update_query = $conn->query("UPDATE users SET activated='$activated' WHERE username='$user[username]'");

						echo "<h1>Congratulations! Your account has just been activated!</h1>";
						echo "<h2>You can now <a href='index.php?a=login'>log in</a>!</h2>";
						exit;
					} else {
						echo "<h1>The code you provided is wrong! It has probably expired!</h1>";
						echo "<h2>You can be sent a new activation email <a href='email_activationcode.php'>here</a>!</h2>";
					}
				}	else {
					echo "<h1>Sorry, your activation code wasn't found in the database!</h1>";
					echo "<h2>You can be sent a new activation email <a href='email_activationcode.php'>here</a>!</h2>";
				}
			} else {
				echo "<h1>Your account is already activated!</h1>";
				echo "<h2>You can <a href='index.php?a=login'>log in</a>!</h2>";
			}
		} else {
			echo "<h1>The account registered with this email address is shut down!</h1>";
			echo "<h2>We are sorry for that! You can <a href='index.php?a=signup'>create another account</a>!</h2>";
		}
	} else {
		echo "<h1>User does not exists! Please check your informations!</h1>";
		echo "<h2>Not registered yet? <a href='index.php?a=signup'>Create an account</a>!</h2>";
	}
} else {
	echo "<h1>Your url didn't provide the necessary tokens to activate the account!</h1>";
	echo "<h2>You can be sent a new activation email <a href='email_activationcode.php'>here</a>!</h2>";
}
?>

</div>

<?php include("footer.php"); ?>
