<?php include('header.php'); ?>

<div class="info">
<?php
$_SESSION['fbmsg']['recoveryfb']='';
$_SESSION['fbmsg']['recoveryerr']='';
if (isset($_POST['reset'])) {
	$pass = recursive_hash($_POST['pass']); // Long recursive hashing process to slow down and avoid password cracking.
	unset($_POST['pass']);
	$pass_hash = password_hash($pass, PASSWORD_DEFAULT); // Creates a new password hash using a strong one-way hashing algorithm.

  $username = $_POST['uname'];

  $query = $conn->prepare("UPDATE users SET password = ? WHERE username = ?") or die($conn->error);
  $query->execute([$pass_hash, $username]);
  $_SESSION['passupdated']=true;

  header("Location: index.php?a=login");
  exit;
}
?>
</div>

<?php include('footer.php'); ?>
