<?php
if (!isset($_SESSION['temp'])) die();
switch (isset($_SESSION['temp']['post']['visibility']) ? $_SESSION['temp']['post']['visibility'] : '') {
  case 'friends':
    $visibility = 'Friends';
    break;
  case 'ffriends':
    $visibility = 'Friends of friends';
    break;
  case 'public':
    $visibility = 'Public';
    break;
  default:
    $visibility = '';
    break;
}
if (isset($_SESSION['temp']['post']['body'])) {
  $body = $_SESSION['temp']['post']['body'];
  $body = str_format($body,"@",["<a href='profile.php?user=","'>","</a>"]);
  $body = str_format($body,"#",["<a href='search.php?searchtype=subject&search=","'>","</a>"],1);

}
if (isset($_SESSION['temp']['post']['path'])) {
  $path = $_SESSION['temp']['post']['path'];
  $files = scandir($path);
  $_SESSION['temp']['post']['files'] = array();
  foreach ($files as $file) {
    array_push($_SESSION['temp']['post']['files'], $file);
  }
  $finfo = finfo_open(FILEINFO_MIME_TYPE);
  $files = array();
  $types = array();
  foreach ($_SESSION['temp']['post']['files'] as $file) {
    array_push($files, $path.$file);
    array_push($types,finfo_file($finfo, $path.$file));
  }
  finfo_close($finfo);
  $filelinks = array();
  foreach ($types as $id => $type) {
    switch (substr($type,0,((strpos($type,"/"))===false) ? strlen($type) : strpos($type,"/"))) {
      case 'text':
        array_push($filelinks,'<p class="item">'.htmlentities(file_get_contents($files[$id])).'</p>');
        break;
      case 'image':
        array_push($filelinks,'<img src="'.$files[$id].'"/>');
        break;
      case 'video':
        array_push($filelinks,'<video controls><source src="'.$files[$id].'" type="'.$type.'">Your browser does not support the video element.</video>');
        break;
      case 'audio':
        array_push($filelinks,'<audio controls><source src="'.$files[$id].'" type="'.$type.'">Your browser does not support the audio element.</audio>');
        break;
      case 'directory':
        array_push($filelinks,'');
        break;
      default:
        array_push($filelinks,'<p><a class="filelink" href="'.$files[$id].'">'.$_SESSION['temp']['post']['files'][$id].'</a></p>');
        break;
    }
  }
}
$comments = array();
foreach ($_SESSION['temp']['post_comments'] as $comment) {
  $comment['body'] = str_format($comment['body'],"@",["<a href='profile.php?user=","'>","</a>"]);
  $comment['body'] = str_format($comment['body'],"#",["<a href='search.php?searchtype=subject&search=","'>","</a>"],1);
  array_push($comments, $comment);
}
?>

<div id='post<?php echo $_SESSION['temp']['post']['id']; ?>'>
<div class='box boxform postitem'>
  <h3>
    <?php
      echo (($_SESSION['temp']['post']['posted_by'][0]!='@') ? ("<i>".$_SESSION['temp']['post']['posted_by']."</i>") : ("<a href='profile.php?user=".$_SESSION['temp']['post']['posted_by']."'>".$_SESSION['temp']['post']['posted_by']."</a>")).(($_SESSION['temp']['post']['posted_to'] != $_SESSION['temp']['post']['posted_by']) ? (' &#x25BB; '.(($_SESSION['temp']['post']['posted_to'][0]!='@') ? ("<i>".$_SESSION['temp']['post']['posted_to']."</i>") : ('<a href=\'profile.php?user='.$_SESSION['temp']['post']['posted_to'].'\'>'.$_SESSION['temp']['post']['posted_to'].'</a>'))) : "");
    ?>
  </h3>
  <h4><?php echo $_SESSION['temp']['post']['date_added'].' - '.$visibility; ?></h4>
  <?php
    if (isset($body)) echo '<p class="subpart">'.$body.'</p>';
    if (isset($filelinks)) {
      foreach ($filelinks as $key => $filelink) {
        if ($filelink != "") {
          echo "<div class='file'>";
          echo $filelink;
          echo "<h5>File Name : <a href='".$files[$key]."'>".$_SESSION['temp']['post']['files'][$key]."</a><br/> Type : ".$types[$key]."<br/>  Path : ".$files[$key]."</h5>";
          echo "</div><br/>";
        }
      }
    }
  ?>
  <?php
    $l = explode(',',$_SESSION['temp']['post']['likes']);
    unset($l[count($l)-1]);
    $c = count($comments);
    if (isset($_SESSION['username'])) {
      echo '
      <div class="lik"><div class="interact like plike '.((strpos($_SESSION['temp']['post']['likes'],$_SESSION['username'].",")===false) ? '' : 'liked').'" onclick="like_post(\''.$_SESSION['temp']['post']['id'].'\');
      setTimeout(function(){$(\'#post'.$_SESSION['temp']['post']['id'].' .lik\').load(document.URL+\' #post'.$_SESSION['temp']['post']['id'].' .plike\'), 100});">Like ('.count($l).')</div></div>
      <div class="interact comment" onclick="$(this).parents(\'.postitem\').find(\'textarea\').val(\'\'); $(this).parents(\'.postitem\').find(\'textarea\').focus();">Comment ('.$c.')</div><br/>';
    }
    else {
      echo '<h5 style="text-align: right;"><span class="like liked">'.count($l).' like'.(count($l)>1 ? 's' : '').'</span> - <span class="comment">'.$c.' comment'.($c>1 ? 's' : '').'</span></h5>';
    }
    if ($c) echo '<div class="commentstoggle bblockdiv subh" onclick="optToggle($(this), $(this).parent().find(\'.comments\'), \'Show comments\', \'Hide comments\')">Show comments</div>';
  ?>
  <div class="com">
  <div class="comments">
    <?php
    foreach ($comments as $comment) {
      echo '<div class="item">';
      echo "<h4><a href='profile.php?user=".$comment['posted_by']."'>".$comment['posted_by']."</a></h4>";
      echo "<h5>".$comment['date_added']."</h5>";
      echo "<p>".$comment['body']."</p>";
      if (isset($_SESSION['username'])) echo '
        <div class="interact like '.((strpos($comment['likes'],$_SESSION['username'].",")===false) ? '' : 'liked').'" onclick="likecomment_post(\''.$_SESSION['temp']['post']['id'].'\',\''.$comment['id'].'\');
        setTimeout(function(){$(\'#post'.$_SESSION['temp']['post']['id'].' .com\').load(document.URL+\' #post'.$_SESSION['temp']['post']['id'].' .comments\', function(){optToggle($(this).find(\'commentstoggle\'), $(this).find(\'.comments\'), \'Show comments\', \'Hide comments\', 1)}), 100});"> '.(count(explode(',',$comment['likes'])) - 1).' likes</div>
        <div class="interact comment" onclick="$(this).parents(\'.postitem\').find(\'textarea\').val(\'▻ '.$comment['posted_by'].' \\n\\n\'); $(this).parents(\'.postitem\').find(\'textarea\').focus();">Reply</div><br/>';
      else echo '<h5 style="text-align: right;"><span class="like liked">'.count(explode(',',$comment['likes'])).' like'.(count(explode(',',$comment['likes']))>1 ? 's' : '').'</span></h5>';
      echo '</div>';
    }
    ?>
  </div>
  </div>

  <?php
	if (isset($_SESSION['username'])) echo '<textarea placeholder="Write something to comment..."></textarea><div class="ibblockdiv esubh" onclick="if (comment_post(\''.$_SESSION['temp']['post']['id'].'\',$(this).parent().find(\'textarea\').val(),$(this).parent().find(\'.postcommentfb\'))!==false) setTimeout(function(){$(\'#post'.$_SESSION['temp']['post']['id'].'\').load(document.URL+\' #post'.$_SESSION['temp']['post']['id'].'\', function(){optToggle($(this).parents().find(\'.commentstoggle\'), $(this).find(\'.comments\'), \'Show comments\', \'Hide comments\')}), 100});">Comment</div><div class="fbmsg postcommentfb"></div>';
  ?>
</div>
</div>
