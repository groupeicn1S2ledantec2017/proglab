<?php include('header.php'); ?>

<?php
$_SESSION['fbmsg']['signupmsg']='';
$_SESSION['fbmsg']['signupfb']='';
$_SESSION['fbmsg']['em_used']=false;
$_SESSION['fbmsg']['em_invalid']=false;
$_SESSION['fbmsg']['un_used']=false;
if (isset($_POST['signup'])) {
	$fname = htmlentities(substr($_POST['fname'], 0, 16)); // Convert all applicable characters to HTML entities after substringing the 16 first chars if the input (if the browser doesn't support maxlength)
	$fname = preg_replace('/\s+/', ' ', $fname); // Remove extra whitespaces
	$fname = mb_convert_case($fname, MB_CASE_TITLE, "UTF-8"); // By contrast to the standard case folding functions such as strtolower() and strtoupper(), case folding is performed on the basis of the Unicode character properties.

	$lname = htmlentities(substr($_POST['lname'], 0, 16));
	$lname = preg_replace('/\s+/', ' ', $lname);
	$lname = mb_convert_case($lname, MB_CASE_TITLE, "UTF-8");

	$pseudo = htmlentities(substr($_POST['pseudo'], 0, 16));
	$pseudo = preg_replace('/\s+/', ' ', $pseudo);

	$uname = '@'.htmlentities(strtolower($_POST['uname']));

	$email = htmlentities(strtolower($_POST['email']));

	if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
		$_SESSION['fbmsg']['signupmsg']="Invalid email address!";
		$_SESSION['fbmsg']['em_invalid']=true;
	}

	$bday = $_POST['byear']."-".((strlen($_POST['bmonth'])<2) ? '0'.$_POST['bmonth'] : $_POST['bmonth'])."-".((strlen($_POST['bday'])<2) ? '0'.$_POST['bday'] : $_POST['bday']);

	$gender = $_POST['gender'];

	$pass = recursive_hash($_POST['pass']); // Long recursive hashing process to slow down and avoid password cracking.
	unset($_POST['pass']);
	$pass = password_hash($pass, PASSWORD_DEFAULT); // Creates a new password hash using a strong one-way hashing algorithm.

	$l_act = @date("Y-m-d H:i:s");

	$verif = $conn->query("SELECT * FROM users WHERE username='$uname'");
	if($verif->fetch()) {
		$_SESSION['fbmsg']['signupmsg']="Username already taken!";
		if ($_SESSION['fbmsg']['em_invalid']) $_SESSION['fbmsg']['signupmsg']="Email address not valid and username already taken!";
		$_SESSION['fbmsg']['un_used']=true;
	}
	$verif = $conn->query("SELECT * FROM users WHERE email='$email'");
	if($verif->fetch()) {
		$_SESSION['fbmsg']['signupmsg']="A user already exists with this email address!";
		if ($_SESSION['fbmsg']['un_used']) $_SESSION['fbmsg']['signupmsg']="A user already exists with this email address and username!";
		$_SESSION['fbmsg']['em_used']=true;
	}

	if (!$_SESSION['fbmsg']['em_used'] && !$_SESSION['fbmsg']['un_used'] && !$_SESSION['fbmsg']['em_invalid']) {
		$initialval = 0;
		$query = $conn->prepare("INSERT INTO users (username, firstname, lastname, pseudo, email, password, birthday, gender, last_activity, activated, shutdown) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)") or die($conn->error);
 		$query->execute([$uname, $fname, $lname, $pseudo, $email, $pass, $bday, $gender, $l_act, $initialval, $initialval]);

		$activation_code = random_hexstring(16);
		$_activationCode = new ActivationCode($uname, $email);

		$status = $_activationCode->send($activation_code);

		if ($status) {
			$_SESSION['fbmsg']['signupfb']="Registration succeded and activation email sent successfully! You can now check your emails to validate your account and then <a href='index.php?a=login'>log in</a>!<br/><br/><hr/>";
		} else {
			$_SESSION['fbmsg']['signupfb']="Registration succeded but activation email could not be sent! You can be sent a new activation email <a href='email_activationcode.php'>here</a>!<br/><br/><hr/>";
		}

		unset($_POST);
		unset($_SESSION['postval']);

		header("Location: index.php?a=login");
		exit;
	} else {
		$_SESSION['postval']=$_POST;
		header("Location: index.php?a=signup");
		exit;
	}
}
?>

<?php include('footer.php'); ?>
