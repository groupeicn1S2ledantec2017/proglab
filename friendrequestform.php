<div id="friendStatus" style="max-width:100%;width:400px;margin:auto;display:block;">
	<form method='post' action='friend_request.php' onsubmit='return false;'>
		<?php
		$profile=$_SESSION['profile']['username'];

		if ($_SESSION["username"] != $profile) {
			$friend_request_query = $conn->query("SELECT * FROM friend_requests WHERE ((sent_by='$_SESSION[username]' AND sent_to='$profile') OR (sent_by='$profile' AND sent_to='$_SESSION[username]'))");
			$result = $friend_request_query->fetch();

			if (!((strpos($_SESSION["friends"], $profile.',')==false) && (strpos($_SESSION["friends"], $profile.',')!==0))) echo '
				<div class="blockdiv asub">You are friends</div>
				<input class="csubh" type="submit" name="requestfriend" onclick="dialog(\'yesno\', \'Unfriend\', \'Do you really want to unfriend this user?\', function(){friend_request(\'remove\', \''.$profile.'\');$(\'#hfriends\').text(\'Friends ('.(count(explode(',',$_SESSION['profile']["friends"]))-1-1).')\');$(\'#youfriend\').remove();});" value="Unfriend"/>';
			else if (!$result) echo '<input type="submit" name="requestfriend" onclick="dialog(\'yesno\', \'Send friend request\', \'Do you really want to send a friend request this user?\', function(){friend_request(\'send\', \''.$profile.'\')});" value="Send friend request"/>';
			else if ($result['sent_by']==$_SESSION["username"] && $result['rejected']) echo '<div class="blockdiv dsub">Your friend request was rejected</div>';
			else if ($result['sent_to']==$_SESSION["username"] && $result['rejected']) echo '
				<div class="blockdiv dsub">You rejected the friend request</div>
				<input class="asubh" type="submit" name="requestfriend" onclick="dialog(\'yesno\', \'Accept friend request\', \'Do you really want to accept the friend request?\', function(){friend_request(\'accept\', \''.$profile.'\');$(\'#hfriends\').text(\'Friends ('.(count(explode(',',$_SESSION['profile']["friends"]))-1+1).')\');$(\'#friends\').append(\'<a id=\\\'youfriend\\\' class=\\\'fbblockdiv sub\\\'>You</a>\');});" value="Accept friend request"/>';
			else if ($result['sent_to']==$_SESSION["username"]) echo '
				<table>
					<tr>
						<td colspan="2"><div class="blockdiv">Friend request received (pending)</div></td>
					</tr>
					<tr>
						<td><input style="margin:0 auto;" class="asubh" type="submit" name="requestfriend" onclick="dialog(\'yesno\', \'Accept friend request\', \'Do you really want to accept the friend request?\', function(){friend_request(\'accept\', \''.$profile.'\');$(\'#hfriends\').text(\'Friends ('.(count(explode(',',$_SESSION['profile']["friends"]))-1+1).')\');$(\'#friends\').append(\'<a id=\\\'youfriend\\\' class=\\\'fbblockdiv sub\\\'>You</a>\');});" value="Accept"/></td>
						<td><input style="margin:0 auto;" class="csubh" type="submit" name="requestfriend" onclick="dialog(\'yesno\', \'Reject friend request\', \'Do you really want to reject the friend request?\', function(){friend_request(\'reject\', \''.$profile.'\')});" value="Reject"/></td>
					</tr>
				</table>';
			else if ($result['sent_by']==$_SESSION["username"]) echo '
				<div class="blockdiv">Friend request sent (pending)</div>
				<input class="bsubh" type="submit" name="requestfriend" onclick="dialog(\'yesno\', \'Cancel friend request\', \'Do you really want to cancel the friend request you sent to this user?\', function(){friend_request(\'cancel\', \''.$profile.'\')});" value="Cancel friend request"/>';
			else echo '<div class="blockdiv">Request status unknown</div>';
		}
		?>
	</form>
</div>
