<?php include('header.php'); ?>

<?php
header('Content-Type: text/html; charset=utf-8');

$_SESSION['fbmsg']['emcheckscsmsg']="";
$_SESSION['fbmsg']['emcheckerrmsg']="";
$_SESSION['fbmsg']['emchecksolmsg']="";
if (isset($_POST['sendemail'])) {
	$email = $_POST['email'];

	$query = $conn->prepare("SELECT id,email,activated,blocked FROM users WHERE email = ?");
	$query->execute([$email]);
	$user = $query->fetch();

	if (isset($user)) {
		if (!$user['shutdown']) {
			if ($user['activated']) {
				$recovery_code = random_hexstring(32);
				$_recoveryCode = new RecoveryCode($user['id'], $user['email']);

				$status = $_recoveryCode->send($recovery_code);

				if ($status) {
					$_SESSION['fbmsg']['emcheckscsmsg']="Account recovery email sent successfully! You can now check your emails to reset your password and then <a href='index.php#login'>log in</a>!";
					$_SESSION['fbmsg']['emchecksolmsg']="Didn't receive an account recovery email? <a href='email_recoverycode.php'>Send again</a>!";
				} else {
					$_SESSION['fbmsg']['emcheckerrmsg']="Recovery email not sent! Something went wrong!";
					$_SESSION['fbmsg']['emchecksolmsg']="Please <a href='email_activationcode.php'>try again</a> later!";
				}

			}	else {
				$_SESSION['fbmsg']['emcheckerrmsg']="The account registered with this email address is not activated!";
				$_SESSION['fbmsg']['emchecksolmsg']="Didn't receive an activation email? <a href='email_activationcode.php'>Send again</a>!";
			}
		} else {
			$_SESSION['fbmsg']['emcheckerrmsg']="The account registered with this email address is shut down!";
			$_SESSION['fbmsg']['emchecksolmsg']="We are sorry for that! You can <a href='index.php#signup'>create another account</a>!";
		}
	} else {
		$_SESSION['fbmsg']['emcheckerrmsg']="No user with the email you entered in the database! Please check your informations!";
		$_SESSION['fbmsg']['emchecksolmsg']="Not registered yet? <a href='index.php#signup'>Create an account</a>!";
	}

	header("Location: email_recoverycode.php");
	exit;
}
?>

<?php include('footer.php'); ?>
