// Date Field
function validYears(selectField) {
    var cDate = new Date();
    var field = document.getElementById(selectField);
    var cYear = cDate.getFullYear();
    for (var i = 0; i<120; i++) {
        var option = document.createElement("option");
        option.text = cYear-i;
        option.value = cYear-i;
        if (i==18) option.selected=true;
        field.add(option);
    }
}
function validMonth(selectField) {
    var field = document.getElementById(selectField);
    var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    for (var i = 0; i<12; i++) {
        var option = document.createElement("option");
        option.text = month[i];
        option.value = i+1;
        if (i===0) option.selected=true;
        field.add(option);
    }
}
function validDays(yearField, monthField, selectField) {
    var year = document.getElementById(yearField).value;
    var month = document.getElementById(monthField).value;
    var field = document.getElementById(selectField);
    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) monthLength[1] = 29;
    while (field.length > monthLength[month-1]) {
        field.remove(field.length-1);
    }
    while (field.length < monthLength[month-1]) {
        var option = document.createElement("option");
        option.text = field.length+1;
        option.value = field.length+1;
        if (field.length===0) option.selected=true;
        field.add(option);
    }
}

// Input Validation
function checkEmail() {
	var email = $('#email').val();

	if ($('#email').val() == 0) {
		$('#email').css('border-color', '');
		$('#email').css('outline-color', '');
		$('#emailfb').css('color', "#000");
		$('#emailfb').text("");
		return false;
	}
	if (email.length > 254) {
		$('#email').css('border-color', "#FF0000");
		$('#email').focus();
		$('#email').css('outline-color', '#FF0000');
		$('#emailfb').css('color', "#FF0000");
		$('#emailfb').text("The email should have a maximal length of 254 characters!");
		return false;
	}
	if (!email.match(/\S+@\S+/)) {
		$('#email').css('border-color', "#FF0000");
		$('#email').focus();
		$('#email').css('outline-color', '#FF0000');
		$('#emailfb').css('color', "#FF0000");
		$('#emailfb').text("The email should contain an @ symbol following the local part and followed by the domain!");
		return false;
	} else {
		$('#email').css('border-color', '');
		$('#email').css('outline-color', '');
		$('#emailfb').css('color', "#0000FF");
		$('#emailfb').text("A verification email will be sent to : "+email);
		return true;
	}
}
function checkUsername() {
	var uname = $('#uname').val();

	if ($('#uname').val() == 0) {
		$('#uname').css('border-color', '');
		$('#uname').css('outline-color', '');
		$('#unamefb').css('color', "#000");
		$('#unamefb').text("");
		return false;
	}

	if (uname.match(/[^(a-z0-9_)]/)) {
		$('#uname').css('border-color', "#FF0000");
		$('#uname').focus();
		$('#uname').css('outline-color', '#FF0000');
		$('#unamefb').css('color', "#FF0000");
		$('#unamefb').text("The username should be alphanumeric and lowercase (a-z0-9_)!");
		return false;
	} else {
		$('#uname').css('border-color', '');
		$('#uname').css('outline-color', '');
		$('#unamefb').css('color', "#0000FF");
		$('#unamefb').text("Your username will be : @"+uname);
		return true;
	}
}
function validatePassword() {
	var pass = $('#pass').val();

	if (pass.length == 0) {
		$('#pass').css('border-color', '');
		$('#pass').css('outline-color', '');
		$('#passstrengthfb').css('color', "#0000FF");
		$('#passstrengthfb').text("");
		return false;
	}

	var passStrength = 0;
	if (pass.match(/[0-9]/)) passStrength++;
	if (pass.match(/[a-z]/)) passStrength++;
	if (pass.match(/[A-Z]/)) passStrength++;
	if (pass.match(/[^(A-Za-z0-9)]/)) passStrength++;

	if (passStrength==4 && pass.length>=16) {
		$('#pass').css('border-color', "#00CC99");
		$('#pass').css('outline-color', '#00CC99');
		$('#passstrengthfb').css('color', "#00CC99");
		$('#passstrengthfb').text("Your password is very strong!");
		return true;
	} else if ((passStrength==3 && pass.length>=12) || (passStrength==4 && pass.length>=8)) {
		$('#pass').css('border-color', "#00CC00");
		$('#pass').css('outline-color', '#00CC00');
		$('#passstrengthfb').css('color', "#00CC00");
		$('#passstrengthfb').text("Your password is strong!");
		return true;
	} else if ((passStrength==3 && pass.length>=8) || (passStrength==2 && pass.length>=16)) {
		$('#pass').css('border-color', "#AACC00");
		$('#pass').css('outline-color', '#99CC00');
		$('#passstrengthfb').css('color', "#AACC00");
		$('#passstrengthfb').text("Your password is average!");
		return true;
	} else if ((passStrength==2 && pass.length>=6) || (passStrength==1 && pass.length>=12)) {
		$('#pass').css('border-color', "#FF9900");
		$('#pass').css('outline-color', '#FF9900');
		$('#passstrengthfb').css('color', "#FF9900");
		$('#passstrengthfb').text("Your password is weak!");
		return true;
	} else {
		$('#pass').css('border-color', "#FF0000");
		$('#pass').css('outline-color', '#FF0000');
		$('#passstrengthfb').css('color', "#FF0000");
		$('#passstrengthfb').text("Your password is too weak!");
		$('#pass').focus();
		return false;
	}
}
function checkPasswordsMatch() {
	if ($('#cpass').val() == "" || $('#pass').val() == "") {
		$('#cpass').css('border-color', '');
		$('#cpass').css('outline-color', '');
		$('#passmatchfb').css('color', "#000");
		$('#passmatchfb').text("");
		return false;
	}

	if ($('#pass').val() != $('#cpass').val()) {
		$('#cpass').css('border-color', '#FF0000');
		$('#cpass').css('outline-color', '#FF0000');
		$('#passmatchfb').css('color', '#FF0000');
		$('#passmatchfb').text("Passwords don't match!");
		$('#cpass').focus();
		return false;
	} else {
		$('#cpass').css('border-color', '');
		$('#cpass').css('outline-color', '');
		$('#passmatchfb').text("");
		return true;
	}
}
function checkInputs(formid) {
	var valid = true;

	$("form#"+formid+" :input").each(function(){
		if ($(this).val().length<1) {
			$("#"+formid+"err").text('All fields should be filled in!');
			$(this).css('border-color', '#FF0000');
			$(this).css('outline-color', '#FF0000');
			$(this).focus();
			valid = false;
		}
	});
	if (formid == 'signup') {
    	$("form#"+formid+" :input[type='text']").each(function(){
    		if ($(this).val().length>16) {
    			$("#"+formid+"err").text('Text inputs (firstname, lastname, pseudo and username) should have a maximal length of 16 characters!');
    			$(this).css('border-color', '#FF0000');
    			$(this).css('outline-color', '#FF0000');
    			$(this).focus();
    			valid = false;
    		}
    	});
	}
	return valid;
}

// Users connections
function friend_request(action, to) {
    var xhr = new XMLHttpRequest();

    xhr.open("POST", "friend_request.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("action="+action+"&to="+to);

    setTimeout(function(){$('#friendStatus').load(document.URL+' #friendStatus')}, 100);
}
function follow_user(action, to, followings, followers) {
  var xhr = new XMLHttpRequest();

  xhr.onload = function () {
  	var data = this.responseText;
    console.log(data);
  }

  xhr.open("POST", "follow_user.php", true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send("action="+action+"&to="+to+"&followings="+followings+"&followers="+followers);

  setTimeout(function(){$('#followStatus').load(document.URL+' #followStatus')}, 100);
}

// Posts
function send_post(body, visibility, to, fb) {
  var xhr = new XMLHttpRequest();
  var feedback = $('#'+fb);

  if (body=="") {
  	feedback.text("Void textarea! Please type something before posting!");
  	feedback.css("color", "red");
  	return false;
  }
  xhr.onload = function () {
  	var data = this.responseText;
  	console.log(data);
  	if (data == "") {
  		feedback.text("Unexpected error occur while sending the post!");
  		feedback.css("color", "red");
  	}
  	else {
  		feedback.text("Text posted successfully!");
  		feedback.css("color", "green");
  	}
  }
  body = encodeURIComponent(body);
  visibility = encodeURIComponent(visibility);
  to = encodeURIComponent(to);
  xhr.open("POST", "send_post.php", true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send("body="+body+"&to="+to+"&visibility="+visibility);
}
function changePostMode(oldbutton, newbutton, oldmode, newmode) {
	$('#'+oldmode).css('display', 'none');
	$('#'+newmode).css('display', 'block');

	$('#'+oldbutton).addClass('subh');
	$('#'+oldbutton).removeClass('dsub');
	$('#'+newbutton).addClass('dsub');
	$('#'+newbutton).removeClass('subh');

	$('#'+newmode+' input[type=radio]').filter(':first').prop('checked', true);
}
function fileInputText(fileinput, fileinputfb) {
	var input = $('#'+fileinput);
	var feedback = "";
	var files = $.map(input.prop("files"), function(val) { return val.name; });

	if (files && files.length > 1) feedback = files.length+" files chosen : \n"+files.join(" \n");
	else if (files && files.length > 0) feedback = "1 file chosen : \n"+files[0];
	else feedback = "No file chosen";

	$('#'+fileinputfb).text(feedback);
}
function supportAdvancedUpload(form, feedback) {
	var div = document.createElement('div');
	if (!(('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window) {
		$('#'+form).hide();
		$('#'+feedback).show();
		return false;
	} else return true;
}
function fileInputDragndrop(input, label) {
	var dropzone = document.getElementById(label);
	var input = document.getElementById(input);

	dropzone.classList.add("dragfile");
	dropzone.ondragover = function() {dropzone.classList.add("dragover"); return false;};
	dropzone.ondragleave = function() {dropzone.classList.remove("dragover"); return false;};
	dropzone.ondrop = function (e) {e.preventDefault(); dropzone.classList.remove("dragover"); input.files = e.dataTransfer.files;};
}
function send_postfile(input, description, visibility, to, prog, status, abort, feedback) {
	var formData = new FormData();
	var xhr = new XMLHttpRequest();

	var files = document.getElementById(input).files;
	var totalSize = 0;

	for (var x=0; x<files.length; x++) {
		console.log(files[x].name+" "+files[x].size+" "+files[x].type);
		formData.append('postfiles[]', files[x]);
		totalSize += files[x].size;
	}
	formData.append("description", description);
	formData.append("visibility", visibility);
	formData.append("to", to);

	var feedback = $("#"+feedback);
	feedback.text("");

	if (files.length > 20) {
		feedback.text("Cannot send more than 20 files in one post!");
		feedback.css("color", "red");
		return false;
	}
	if (files.length < 1) {
		feedback.text("No file selected!");
		feedback.css("color", "red");
		return false;
	}
	if (totalSize > 96 * 1048576) {
		feedback.text("Files too big for one post (>96 Mb)!");
		feedback.css("color", "red");
		return false;
	}

	xhr.upload.addEventListener("progress", progressHandler, false);
	xhr.addEventListener("load", completeHandler, false);
	xhr.addEventListener("error", errorHandler, false);
	xhr.addEventListener("abort", abortHandler, false);

	function progressHandler(e) {
		$("#"+status).text("Uploading... (uploaded "+(Math.round((e.loaded / e.total) * 10000)/100)+"% ("+e.loaded+" bytes of "+e.total+"))");
		$("#"+prog).val(Math.round((e.loaded / e.total) * 100));
	}
	function completeHandler(e) {
		$("#"+status).text("Upload complete!");
		$("#"+abort).css('display', 'none');
		$("#"+prog).css('display', 'none');
		return false;
	}
	function abortHandler(e) {
		$("#"+status).text("Upload aborted!");
		$("#"+abort).css('display', 'none');
		$("#"+prog).css('display', 'none');
		return false;
	}
	function errorHandler(e) {
		$("#"+status).text("Upload failed!");
		$("#"+abort).css('display', 'none');
		return false;
	}

	xhr.onload = function () {
		var data = this.responseText;
		console.log(data);
		if (data === "") {
			feedback.text("Unexpected error occur while sending the post!");
			feedback.css("color", "red");
		}
		else {
			feedback.text("File posted successfully!");
			feedback.css("color", "green");
		}
	}

	$("#"+prog).css('display', 'block');
	$("#"+abort).css('display', 'block');
	$("#"+abort).on("click", function(e) {xhr.abort();});

	xhr.open('post', 'send_postfile.php');
	xhr.send(formData);
}
function like_post(id) {
  var xhr = new XMLHttpRequest();

  xhr.onload = function () {
  	var data = this.responseText;
  	console.log(data);
  }
  id = encodeURIComponent(id);
  xhr.open("POST", "react_post.php", true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send("action=like&id="+id);
}
function likecomment_post(id, cid) {
  var xhr = new XMLHttpRequest();
  
  xhr.onload = function () {
  	var data = this.responseText;
  	console.log(data);
  }
  id = encodeURIComponent(id);
  cid = encodeURIComponent(cid);
  xhr.open("POST", "react_post.php", true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send("action=likecomment&id="+id+"&cid="+cid);
}
function comment_post(id, body, feedback) {
  var xhr = new XMLHttpRequest();

  if (body=="") {
  	feedback.text("Void textarea! Please type something before commenting!");
  	feedback.css("color", "red");
  	return false;
  }
  xhr.onload = function () {
  	var data = this.responseText;
  	console.log(data);
  	if (data == "") {
  		feedback.text("Unexpected error occur while sending the comment!");
  		feedback.css("color", "red");
  	}
  	else {
  		feedback.text("Comment posted successfully!");
  		feedback.css("color", "green");
  	}
  }
  id = encodeURIComponent(id);
  body = encodeURIComponent(body);
  xhr.open("POST", "react_post.php", true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send("action=comment&id="+id+"&body="+body);
}

//Dialog
function dialog(type, content, scontent="", func1=function(){return false;}, func2=function(){return false;}) {
	var dialogtitle, ui, act;

	switch (type) {
		case 'yesno':
			ui = ['Yes', 'No'];
			act = [func1, func2];
			dialogtitle = document.createTextNode("Dialog");
			break;
		case 'confirm':
			ui = ['Ok', 'Cancel'];
			act = [func1, func2];
			dialogtitle = document.createTextNode("Confirm");
			break;
		case 'alert':
			ui = ['Ok'];
			act = [func1];
			dialogtitle = document.createTextNode("Alert");
			break;
		case 'info': default:
			ui = ['Ok'];
			act = [func1];
			dialogtitle = document.createTextNode("Info");
			break;
	}

	var dialogoverlay = document.createElement('div');
	dialogoverlay.className="overlay";
	document.body.appendChild(dialogoverlay);

	var dialogbox = document.createElement('div');
	dialogbox.className="dialogbox";
	dialogoverlay.appendChild(dialogbox);

	var h2 = document.createElement("h2");
	h2.appendChild(dialogtitle);
	dialogbox.appendChild(h2);

	var h3 = document.createElement("h3");
	h3.appendChild(document.createTextNode(content));
	dialogbox.appendChild(h3);

	var p = document.createElement("p");
	p.appendChild(document.createTextNode(scontent));
	dialogbox.appendChild(p);

	var uitable = document.createElement('table');
	dialogbox.appendChild(uitable);

	var uitr = document.createElement('tr');
	uitable.appendChild(uitr);

	var buttons = [];
	for (var i=0; i<ui.length; i++) {
		inp = document.createElement('input');
		buttons.push(inp);
		buttons[i].value = ui[i];
		buttons[i].type = "submit";
		buttons[i].role = "button";
		buttons[i].onclick = act[i];
		buttons[i].addEventListener('click', function() {setTimeout(function(){dialogoverlay.style.opacity = 0; setTimeout(function(){document.body.removeChild(dialogoverlay);}, 500)}, 200)});
	}
	for (var i=0; i<buttons.length; i++) {
		uitd = document.createElement('td');
		uitd.appendChild(buttons[i]);
		uitr.appendChild(uitd);
	}

	setTimeout(function(){ dialogoverlay.style.opacity = 1;}, 100);
}
function menuToggle(hide, show, button, obj) {
  if ($(show).css('display') == 'none') {
    $(hide).hide();
    $(show).show();
  }
  else $(hide).hide();

  if (obj.hasClass('active')) $(button).removeClass('active');
  else {
    $(button).removeClass('active');
    obj.addClass('active');
  }
}

function dropdown(dl) {
  var button = $('#'+dl+' dt');
  var data = $('#'+dl+' dd');

  if (data.height() < 1) {
    $('.dropdown dd').height(0);
    $('.dropdown dd').css('z-index', '1');
    data.height(300);
    data.css('z-index', '10');
  }
  else {
    setTimeout(function() {$('.dropdown dd').css('z-index', '1');}, 100);
    $('.dropdown dd').height(0);
  }

  if (button.hasClass('dactive')) $('.dropdown dt').removeClass('dactive');
  else {
    $('.dropdown dt').removeClass('dactive');
    button.addClass('dactive');
  }

  $('.dropdown').each(function() {
    dl = $(this);
    var checkedinputs = dl.find(' dd li input:checked');
    if (checkedinputs.length < 1) {
      dl.find('.nothSel').show();
      dl.find('.multiSel').val('');
      dl.find('.multiSel').hide();
      dl.find('.formMultiSel').val('');
    }
    else {
      dl.find('.multiSel').val('');
      dl.find('.formMultiSel').val('');
      checkedinputs.each(function() {
        dl.find('.multiSel').val(dl.find('.multiSel').val() + ", " + $(this).parent().text());
        dl.find('.formMultiSel').val(dl.find('.formMultiSel').val() + " " + $(this).val());
      });
      dl.find('.nothSel').hide();
      dl.find('.multiSel').val(dl.find('.multiSel').val().substr(2));
      dl.find('.formMultiSel').val(dl.find('.formMultiSel').val().substr(1));
      dl.find('.multiSel').show();
    }
  });
}

function navsearchbar() {
    var searchbarwidth = $("nav").width() - 100;
    if (searchbarwidth > 1825) searchbarwidth = 1825;
    $(".navlogo").each(function() { searchbarwidth -= $(this).width() + 25; });
    if ($("nav").width() >= 1000) $(".navmenu").each(function() { searchbarwidth -= 100; });
    else $("#navtoggler").each(function() { searchbarwidth -= $(this).width(); });
    if ($("nav").width() > 1000) $("#navigation").each(function() { $(this).show(); });
    else {
      $("#navigation").each(function() { $(this).hide(); });
      $("#navtoggler button").each(function() { $(this).removeClass('active'); });
    }
    $(".inavmenu").each(function() { searchbarwidth -= $(this).width(); });
    $(".navimage").each(function() { searchbarwidth -= $(this).width() + 25; });
    $(".navtext").each(function() { searchbarwidth -= $(this).width() + 25; });
    $(".navsearch").each(function() { $(this).width(searchbarwidth); });
}
function focusonbox(box) {
  $('#'+box).css('opacity', '1');
	$('#'+box).css('box-shadow', '8px 15px 20px gray');
}

function selecthandle(obj, idtable, optclass) {
  $('.'+optclass).hide();
  $('#'+idtable[obj.prop('selectedIndex')]).show();
}

function searchtypes() {
  selecthandle($('#search select'), ['usersearch', 'friendsearch', 'subjectsearch'], 'searchopt');
  $('#search .searchopt').hide();
  $('#search input').prop('disabled', true);
  $('#search #'+$('#search select').val()+'search').show();
  $('#search #'+$('#search select').val()+'search input').prop('disabled', false);
  $('#search input[type=submit]').prop('disabled', false);
}

function respectRange(min, max, minchange='true') {
  if (!minchange) {
    if (parseInt($('#'+max).val()) < parseInt($('#'+min).val())) $('#'+max).val($('#'+min).val());
    if (parseInt($('#'+min).val()) > parseInt($('#'+max).val())) $('#'+min).val($('#'+max).val());
  } else {
    if (parseInt($('#'+min).val()) > parseInt($('#'+max).val())) $('#'+min).val($('#'+max).val());
    if (parseInt($('#'+max).val()) < parseInt($('#'+min).val())) $('#'+max).val($('#'+min).val());
  }
  if (parseInt($('#'+max).val()) > 120) $('#'+max).val(120);
  if (parseInt($('#'+min).val()) > 120) $('#'+min).val(120);
  if (parseInt($('#'+max).val()) < 0) $('#'+max).val(0);
  if (parseInt($('#'+min).val()) < 0) $('#'+min).val(0);
  $('#'+min).prop('max', $('#'+max).val());
  $('#'+max).prop('min', $('#'+min).val());
}

function optToggle(button, obj, text1, text2,ntrans) {
  if (obj.height() < 1) {
    button.text(text2);
    if (ntrans) {
      obj.css('transition', 'none');
      obj.css('max-height', '500px');
      scrollBottom(obj);
      obj.css('transition', '1s');
    } else {
      obj.css('max-height', '500px');
      scrollBottom(obj);
    }
  }
  else {
    obj.css('max-height', '0');
    button.text(text1);
  }
}

function scrollBottom(obj) {
  obj.scrollTop(obj[0].scrollHeight);
}
