<?php include('main.php'); ?>

<?php
if (isset($_SESSION['username'])) {
  if (isset($_POST) && isset($_POST['body'])) {
    $file = 0;
    $body = $_POST['body'];
    $date_added = date("Y-m-d H:i:s");
    $by = $_SESSION['username'];
    $to = $_POST['to'];
    $visibility = $_POST['visibility'];
    $likes = '';

    $query = $conn->prepare("INSERT INTO posts (file, body, date_added, posted_by, posted_to, visibility, likes) VALUES (?, ?, ?, ?, ?, ?, ?)");
    $query->execute([$file, $body, $date_added, $by, $to, $visibility, $likes]);

    $uploaded[] = array('text' => $body);
    echo json_encode($uploaded);
  }
}
?>
