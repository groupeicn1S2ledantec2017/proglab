<?php include('main.php'); ?>

<?php
if (isset($_SESSION['username'])) {
	$l_act = @date("Y-m-d H:i:s");
	$up_query = $conn->query("UPDATE users SET last_activity='$l_act' WHERE username='$_SESSION[username]'") or die ("Couldn't update last_activity : ".$conn->error);
}
?>

<!DOCTYPE HTML>
<html>
	<head>
			<link rel="stylesheet" type="text/css" href="css/style.css<?php echo'?v='.time();?> "> <!-- the php statement has to be commented on the social network last version not ot loose style caching -->
			<meta charset="UTF-8">
			<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
			<script type="text/javascript" src="js/script.js<?php echo'?v='.time();?>"></script>  <!-- the php statement has to be commented on the social network last version not ot loose style caching -->
			<title>ProgLab</title>
			<link rel="shortcut icon" href="media/favicon.png">
	</head>

	<body onunload="">

		<nav>
			<ul>
				<li class="navlogo"><img src="media/favicon.png" alt="logo" /></li>
				<?php
				if (isset($_SESSION['username'])) {
					if (isset($_SESSION['profilepic']) && isset($_SESSION['profilepic'])!="") $profilepicpath = $_SESSION['profilepic'];
					else $profilepicpath = 'media/profilepic.jpg';
					echo '<li class="navimage"><div style="background-image: url('.$profilepicpath.');"></div></li>';
					echo '<li class="navtext"><input class="readonly" type="text" value="'.$_SESSION['username'].'" readonly /></li>';
				}
				?>
				<li class="navsearch">
					<form action="search.php" method="get">
						<input type="search" name="search" placeholder="Search..." value="" />
						<select name="searchtype">
							<option value="user">User</option>
							<option value="friend">Friend</option>
							<option value="subject">Subject</option>
						</select>
					</form>
				</li>
				<?php
				if (isset($_SESSION['username'])) {
					echo '
					<style>@media screen and (max-width: 800px) {.navsearch {display: none;}}</style>
					<div id="navtoggler" class="navtoggler"><button onclick="menuToggle(\'#navigation\', \'#navigation\', $(this), $(this));"><div class="navicon">&#9776;</div></button></div>
					<div id="navigation">
						<li class="navmenu"><a href="home.php" style="background-color: blue;">Home</a></li>
						<li class="navmenu">
							<dl>
								<dt onclick="menuToggle(\'nav li dd\', \'#new\', \'nav li dt\', $(this));">New</dt>
								<dd id="new">
									<ul>
										<li><a href="">New Chat</a></li>
										<li><a href="">New Group</a></li>
										<li><a href="search.php?searchtype=user">Find User</a></li>
										<li><a href="search.php?searchtype=friend">Find Friends</a></li>
										<li><a href="search.php?searchtype=subject">Find Subject</a></li>
									</ul>
								<dd>
							</dl>
						</li>
						<li class="navmenu">
							<dl>
								<dt onclick="menuToggle(\'nav li dd\', \'#inbox\', \'nav li dt\', $(this));">Inbox</dt>
								<dd id="inbox">
									<ul>
										<li><a href="">Notifications</a></li>
										<li><a href="">Discussions</a></li>
										<li><a href="">Groups</a></li>
										<li><a href="">Friend Requests</a></li>
									</ul>
								<dd>
							</dl>
						</li>
						<li class="navmenu">
							<dl>
								<dt onclick="menuToggle(\'nav li dd\', \'#account\', \'nav li dt\', $(this));">Account</dt>
								<dd id="account">
									<ul>
										<li><a href="profile.php">Profile</a></li>
										<li><a href="settings.php">Settings</a></li>
										<li><a href="logout.php">Log out</a></li>
									</ul>
								<dd>
							</dl>
						</li>
					</div>';
				} else {
					if (isset($_GET['a'])) {
						if ($_GET['a']=='login') {
							echo '<li class="inavmenu"><a href="index.php">Index</a></li>';
							echo '<li class="inavmenu"><a href="index.php?a=signup">Sign Up</a></li>';
						} else if ($_GET['a']=='signup') {
							echo '<li class="inavmenu"><a href="index.php">Index</a></li>';
							echo '<li class="inavmenu"><a href="index.php?a=login">Log In</a></li>';
						} else {
							echo '<li class="inavmenu"><a href="index.php?a=login">Log In</a></li>';
							echo '<li class="inavmenu"><a href="index.php?a=signup">Sign Up</a></li>';
						}
					} else {
						echo '<li class="inavmenu"><a href="index.php?a=login">Log In</a></li>';
						echo '<li class="inavmenu"><a href="index.php?a=signup">Sign Up</a></li>';
					}
				}
				?>
			</ul>
		</nav>

		<script type="text/javascript">
			$(window).click(function(event) {
				if(!$(event.target).parents().hasClass("navmenu")) {
					$('nav li dd').hide();
					$('nav li dt').removeClass('active');
				}
				if ($(window).width() < 1000) {
					if(!$(event.target).parents().hasClass("navtoggler") && !$(event.target).parents().hasClass("navmenu")) {
						$('#navigation').hide();
						$('#navtoggler button').removeClass('active');
					}
				}
			});
			$(window).resize(function() {navsearchbar();});
			$(document).ready(function() {navsearchbar();});
		</script>

		<div id="content">
