<?php include('header.php'); ?>

<?php

if (!isset($_SESSION["username"])) {
	echo '
		<div class="info">
			<h1>You are logged out.</h1><br/>
			<h2>You will be redirected on the index page.</h2>
		</div>
		<meta http-equiv="refresh" content="2; url=index.php" />';
	include ("footer.php");
	die();
}
?>

<h1>Hello <?php echo $_SESSION["firstname"]." ".$_SESSION["lastname"]." (".$_SESSION["pseudo"].")!"?></h1>
<h2>Welcome at your home page!</h2>
<br/>

<?php
new PostForm();
?>

<h3><a href="logout.php">Log Out</a></h3>

<?php
$i = 10;
$i++;

$p_query = $conn->prepare("
	SELECT posts.id,posts.date_added
	FROM
		posts INNER JOIN users ON ((users.followings LIKE CONCAT('%',posts.posted_by,',%') OR users.followings LIKE CONCAT('%',posts.posted_to,',%')) OR (users.username=posts.posted_by OR users.username=posts.posted_to) OR (posts.body REGEXP :run))
	WHERE
		(
			(users.username=:un AND posts.posted_by=:un)
			OR
			(users.username=:un AND posts.posted_to=:un)
			OR
			(users.username=:un AND posts.visibility='public')
			OR
			(users.username=:un AND posts.visibility='ffriends' AND (users.ffriends LIKE CONCAT('%',posts.posted_by,',%') OR users.ffriends LIKE CONCAT('%',posts.posted_to,',%')))
			OR
			(users.username=:un AND posts.visibility='friends' AND (users.friends LIKE CONCAT('%',posts.posted_by,',%') OR users.friends LIKE CONCAT('%',posts.posted_to,',%')))
		)
	ORDER BY date_added DESC -- LIMIT $i");
$regexun = '[[:<:]]@'.substr($_SESSION['username'],1).'[[:>:]]';
$p_query->execute(array("run"=>$regexun, "un"=>$_SESSION['username']));
$posts = array();
$i--;
if ($post = $p_query->fetch()) {
	echo '<div id="posts"><hr/><h3>Timeline</h3><hr/>';
	do {
		$post = new PostItem($post['id']);
		$post->display();
	} while (/*--$i > 0 &&*/ ($post = $p_query->fetch()));
	echo '</div>';
} else {
	echo '<div id="posts"><hr/><h3>Nothing to show</h3><hr/></div>';
}
//if ($p_result->fetch()) echo '<div class="bblockdiv dsubh">Load More</div>';
?>

<?php include('footer.php'); ?>
