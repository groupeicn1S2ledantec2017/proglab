<?php include('main.php'); ?>

<?php
function remove_friend($u, $f) {
	global $conn;

	$query = $conn->query("SELECT friends FROM users WHERE username='$u'");
	$data = $query->fetch();
	echo $conn->error;
	$friends = str_replace($f.",", "", $data['friends']);
	$ffriends = "";
	foreach (explode(',', $friends) as $friend) {
		$friend_query = $conn->query("SELECT friends FROM users WHERE username='$friend'");
		$friend_data = $friend_query->fetch();
		echo $conn->error;
		$ffriends = $ffriends.$friend_data['friends'];
	}
	$ffriends = implode(',',array_unique(explode(',', $ffriends)));
	$uquery = $conn->query("UPDATE users SET friends='$friends', ffriends='$ffriends' WHERE username='$u'");
	echo $conn->error;
	return array($friends, $ffriends);
}
function add_friend($u, $f) {
	global $conn;

	$uquery = $conn->query("SELECT friends,ffriends FROM users WHERE username='$u'");
	$udata = $uquery->fetch();
	echo $conn->error;
	$fquery = $conn->query("SELECT friends FROM users WHERE username='$f'");
	$fdata = $fquery->fetch();
	echo $conn->error;
	$friends = $udata['friends'].$f.',';
	$ffriends = implode(',',array_unique(explode(',', $fdata['friends'].$udata['ffriends'].$u.','.$f.',')));
	$query = $conn->query("UPDATE users SET friends='$friends', ffriends='$ffriends' WHERE username='$u'");
	echo $conn->error;
	return array($friends, $ffriends);
}

if (isset($_SESSION['username'])) {
    if (isset($_POST) && isset($_POST['action'])) {
        $action = $_POST['action'];

        $by = $_SESSION['username'];
        $to = $_POST['to'];

        switch ($action) {
            case 'send':
								$rej = 0;
								$test_query = $conn->query("SELECT * FROM friend_requests WHERE (sent_by='$to' AND sent_to='$by') OR (sent_by='$by' AND sent_to='$to')");
	              if (!($test_query->fetch())) $query = $conn->query("INSERT INTO friend_requests (sent_by, sent_to, rejected) VALUES ('$by', '$to', '$rej')");
	              else echo 'A friend request was already sent to you by this user!';
	              break;
            case 'remove':
                $newdata = remove_friend($by, $to);
                remove_friend($to, $by);
                $_SESSION['friends']=$newdata[0];
                $_SESSION['ffriends']=$newdata[1];
								break;
            case 'accept':
                $delete_query = $conn->query("DELETE FROM friend_requests WHERE (sent_by='$to' AND sent_to='$by') OR (sent_by='$by' AND sent_to='$to')");
                $newdata = add_friend($by, $to);
                add_friend($to, $by);
								$_SESSION['friends']=$newdata[0];
                $_SESSION['ffriends']=$newdata[1];
                break;
            case 'reject':
								$rej = 1;
                $query = $conn->query("UPDATE friend_requests SET rejected='$rej' WHERE (sent_by='$to' AND sent_to='$by')");
                break;
            case 'cancel':
                $delete_query = $conn->query("DELETE FROM friend_requests WHERE (sent_by='$by' AND sent_to='$to')");
                break;
            default:
                $query = $conn->query("DELETE FROM friend_requests WHERE (sent_by='$to' AND sent_to='$by') OR (sent_by='$by' AND sent_to='$to')");
                break;
        }
    }
}
?>

<?php include('footer.php'); ?>
