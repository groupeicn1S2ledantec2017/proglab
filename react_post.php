<?php include('main.php'); ?>

<?php
if (isset($_SESSION['username'])) {
  if (isset($_POST) && isset($_POST['action'])) {
    $post = new PostItem($_POST['id']);
    switch ($_POST['action']) {
      case 'like':
        $post->like();
        echo json_encode([array('(un)liked' => $_POST['id'])]);
        break;
      case 'likecomment':
        $post->likecomment($_POST['cid']);
        echo json_encode([array('(un)liked' => $_POST['id'].".".$_POST['cid'])]);
        break;
      case 'comment':
        $body = htmlentities($_POST['body']);
        $post->comment($body);
        echo json_encode([array('comment' => $body)]);
        break;
    }
  }
}
?>
