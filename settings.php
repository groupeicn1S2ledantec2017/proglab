<?php include('header.php'); ?>

<?php
$_SESSION['fbmsg']="";
if (!isset($_SESSION['username'])) {
  header('Location: home.php');
  exit;
}
if (isset($_POST['changepass'])){
	$old_password = recursive_hash($_POST['opass']);
	$new_password = recursive_hash($_POST['npass']);
	unset($_POST['npass']);
	$new_password = password_hash($new_password, PASSWORD_DEFAULT);

	$req = $conn->prepare('SELECT password FROM users WHERE username=?');
	$req->execute([$_SESSION["username"]]);
	$user = $req->fetch();
	if (password_verify($old_password, $user['password'])){
		$req = $conn->prepare('UPDATE users SET password=? WHERE username=?');
		$req->execute([$new_password, $_SESSION["username"]]);
		session_destroy();
		die('<h1>Your password was just updated!</h1><h3>You will be redirected to the index page.</h3><meta http-equiv="refresh" content="3;url=index.php"/>');
	} else {
		$_SESSION['fbmsg']="Wrong Password!";
	}
}
if (isset($_POST['changeemail'])){
	$new_email = htmlentities($_POST['email']);

	if (filter_var($new_email, FILTER_VALIDATE_EMAIL) === false) {
		$_SESSION['fbmsg']="Invalid email address!";
	} else {
		$verif = $conn->prepare("SELECT * FROM users WHERE email=?");
		$verif->execute([$new_email]);

		if ($verif->fetch()) {
			$_SESSION['fbmsg']="A user already exists with this email address!";
		} else {
			$req = $conn->prepare('UPDATE users SET email=? WHERE username=?');
			$req->execute([$new_email, $_SESSION["username"]]);
			$req = $conn->prepare("UPDATE users SET activated='0' WHERE username=?");
			$req->execute([$_SESSION["username"]]);
      $activation_code = random_hexstring(16);
			$_activationCode = new ActivationCode($_SESSION["username"], $new_email);
			$status = $_activationCode->send($activation_code);
      session_destroy();
			if ($status) {
				die('<h1>Your email address was just updated!</h1><h2>An activation email has just been sent to this address.</h2><h3>You will be redirected to the index page.</h3><meta http-equiv="refresh" content="3;url=index.php"/>');
			} else {
				die('<h1>Your email address was just updated!</h1><h2>However, an error occur while sending the activation email, so please try again later.</h2><h3>You will be redirected to the index page.</h3><meta http-equiv="refresh" content="3;url=index.php"/>');
			}
		}
	}
}
if (isset($_POST['changeuname'])){
	$new_username = '@'.htmlentities(strtolower($_POST['uname']));

	$verif = $conn->prepare("SELECT * FROM users WHERE username=?");
	$verif->execute([$new_username]);

	if ($verif->fetch()) {
		$_SESSION['fbmsg']="Username already taken!";
	} else {
		$req = $conn->prepare('UPDATE users SET username=? WHERE username=?');
		$req->execute([$new_username, $_SESSION["username"]]);
    $req = $conn->prepare('UPDATE posts SET posted_by=? WHERE posted_by=?');
    $req->execute([$new_username, $_SESSION["username"]]);
    $req = $conn->prepare('UPDATE posts SET posted_to=? WHERE posted_to=?');
    $req->execute([$new_username, $_SESSION["username"]]);
    $req = $conn->prepare('UPDATE posts_documents SET posted_by=? WHERE posted_by=?');
    $req->execute([$new_username, $_SESSION["username"]]);
    $req = $conn->prepare('UPDATE posts_documents SET posted_to=? WHERE posted_to=?');
    $req->execute([$new_username, $_SESSION["username"]]);
		session_destroy();
		die('<h1>Your username was just updated!</h1><h3>You will be redirected to the index page.</h3><meta http-equiv="refresh" content="3;url=index.php"/>');
	}
}
if (isset($_POST['changeinfos'])){
	$fname = htmlentities(substr($_POST['fname'], 0, 16));
	$fname = preg_replace('/\s+/', ' ', $fname);
	$fname = mb_convert_case($fname, MB_CASE_TITLE, "UTF-8");

	$lname = htmlentities(substr($_POST['lname'], 0, 16));
	$lname = preg_replace('/\s+/', ' ', $lname);
	$lname = mb_convert_case($lname, MB_CASE_TITLE, "UTF-8");

	$pseudo = htmlentities(substr($_POST['pseudo'], 0, 16));
	$pseudo = preg_replace('/\s+/', ' ', $pseudo);

	$lang = htmlentities(isset($_POST['lang']) ? $_POST['lang'] : '');
	$proglang = htmlentities(isset($_POST['proglang']) ? $_POST['proglang'] : '');
	$progpara = htmlentities(isset($_POST['progpara']) ? $_POST['progpara'] : '');
	$interests = htmlentities(isset($_POST['interests']) ? $_POST['interests'] : '');

	if (!is_numeric(str_replace(" ","",$lang))) $lang='';
	if (!is_numeric(str_replace(" ","",$proglang))) $proglang='';
	if (!is_numeric(str_replace(" ","",$progpara))) $progpara='';
	if (!is_numeric(str_replace(" ","",$interests))) $interests='';

  if (!is_numeric($_POST['byear'])) $_POST['byear']="1970";
  if (!is_numeric($_POST['bmonth'])) $_POST['bmonth']="01";
  if (!is_numeric($_POST['bday'])) $_POST['bday']="01";
	$bday = $_POST['byear']."-".((strlen($_POST['bmonth'])<2) ? '0'.$_POST['bmonth'] : $_POST['bmonth'])."-".((strlen($_POST['bday'])<2) ? '0'.$_POST['bday'] : $_POST['bday']);

  if (!in_array($_POST['gender'], array('male','female','other'))) $_POST['gender']="other";
	$gender = $_POST['gender'];

  $lang = str_replace(" ",",",$lang);
  if ($lang!="") $lang .= ",";
  $proglang = str_replace(" ",",",$proglang);
  if ($proglang!="") $proglang .= ",";
  $progpara = str_replace(" ",",",$progpara);
  if ($progpara!="") $progpara .= ",";
  $interests = str_replace(" ",",",$interests);
  if ($interests!="") $interests .= ",";

	$req = $conn->prepare('UPDATE users SET firstname=?,lastname=?,pseudo=?,gender=?,birthday=?,languages=?,prog_languages=?,prog_paradigms=?,interests=? WHERE username=?');
	$req->execute([$fname, $lname, $pseudo, $gender, $bday, $lang, $proglang, $progpara, $interests, $_SESSION["username"]]);
	session_destroy();
	die('<h1>Your informations were just updated!</h1><h3>You will be redirected to the index page.</h3><meta http-equiv="refresh" content="3;url=index.php"/>');
}
if (isset($_POST['deleteaccount'])) {
	$password = recursive_hash($_POST['pass']);
	$req = $conn->prepare('SELECT id,password FROM users WHERE username=?');
	$req->execute([$_SESSION["username"]]);
	$user = $req->fetch();
	if (password_verify($password, $user['password'])) {
		if (isset($_POST['deletewarning'])) {
			$req = $conn->prepare('DELETE FROM users WHERE username=?');
			$req->execute([$_SESSION["username"]]);
      $un = "deleted_account_".$user['id'];
			if (isset($_POST['deleteposts'])) {
				$req = $conn->prepare('DELETE FROM posts WHERE posted_by=?');
				$req->execute([$_SESSION["username"]]);
        $req = $conn->prepare('DELETE FROM posts_documents WHERE posted_by=?');
				$req->execute([$_SESSION["username"]]);
        $req = $conn->prepare('UPDATE posts SET posted_to=? WHERE posted_to=?');
				$req->execute([$un, $_SESSION["username"]]);
        $req = $conn->prepare('UPDATE posts_documents SET posted_to=? WHERE posted_to=?');
        $req->execute([$un, $_SESSION["username"]]);
				session_destroy();
				die('<h1>Your account and your posts were just deleted!</h1><h2>We hope you enjoyed your experience with this social network.</h2><h3>You will be redirected to the index page.</h3><meta http-equiv="refresh" content="3;url=index.php"/>');
			} else {
        $req = $conn->prepare('UPDATE posts SET posted_by=? WHERE posted_by=?');
				$req->execute([$un, $_SESSION["username"]]);
        $req = $conn->prepare('UPDATE posts_documents SET posted_by=? WHERE posted_by=?');
        $req->execute([$un, $_SESSION["username"]]);
        $req = $conn->prepare('UPDATE posts SET posted_to=? WHERE posted_to=?');
				$req->execute([$un, $_SESSION["username"]]);
        $req = $conn->prepare('UPDATE posts_documents SET posted_to=? WHERE posted_to=?');
        $req->execute([$un, $_SESSION["username"]]);
				session_destroy();
				die('<h1>Your account was just deleted!</h1><h2>We hope you enjoyed your experience with this social network.</h2><h3>You will be redirected to the index page.</h3><meta http-equiv="refresh" content="3;url=index.php"/>');
			}
		} else {
			$_SESSION['fbmsg']="You didn't acknowledge you were aware of the consequences of this action!";
		}
	} else {
		$_SESSION['fbmsg']="Wrong Password!";
	}
}
?>

<script>
  $(document).ready(function() {
    $('.hide').hide();
  	validYears('byear');
  	validMonth('bmonth');
  	validDays('byear', 'bmonth', 'bday');
    <?php
    echo "$('#bday').val('".(int)(substr($_SESSION['birthday'], -2))."');";
    echo "$('#bmonth').val('".substr($_SESSION['birthday'], -4, -3)."');";
    echo "$('#byear').val('".substr($_SESSION['birthday'], 0, 4)."');";
    ?>
    <?php
    foreach (explode(",", $_SESSION['languages']) as $i) echo '$("#l'.$i.'").prop("checked", true);';
    foreach (explode(",", $_SESSION['prog_languages']) as $i) echo '$("#pl'.$i.'").prop("checked", true);';
    foreach (explode(",", $_SESSION['prog_paradigms']) as $i) echo '$("#pp'.$i.'").prop("checked", true);';
    foreach (explode(",", $_SESSION['interests']) as $i) echo '$("#i'.$i.'").prop("checked", true);';
    ?>
    dropdown();
  });
  $(window).click(function(event) { if(!$(event.target).parents().hasClass("dropdown")) $('.dropdown .dactive').parent().each(function() {dropdown($(this).attr('id'));})});
</script>

<h1>Settings</h1>
<br/><br/>
<div class="box">
	<h2>Change settings</h2>
	<hr/>
	<table style="margin:0 auto;padding:0;">
		<tr style="margin:0 auto;padding:0;">
			<td style="margin:15px;width:20%;"><div class="bblockdiv subh" style="padding:15px;" onclick="menuToggle('.hide', '#changePass', '.bblockdiv', $(this));"><p>Change password</p></div></td>
			<td style="margin:15px;width:20%;"><div class="bblockdiv subh" style="padding:15px;" onclick="menuToggle('.hide', '#changeEmail', '.bblockdiv', $(this));"><p>Change email</p></div></td>
			<td style="margin:15px;width:20%;"><div class="bblockdiv subh" style="padding:15px;" onclick="menuToggle('.hide', '#changeUname', '.bblockdiv', $(this));"><p>Change username</p></div></td>
			<td style="margin:15px;width:20%;"><div class="bblockdiv subh" style="padding:15px;" onclick="menuToggle('.hide', '#changeInfo', '.bblockdiv', $(this));"><p>Update informations</p></div></td>
			<td style="margin:15px;width:20%;"><div class="bblockdiv subh" style="padding:15px;" onclick="menuToggle('.hide', '#deleteAccount', '.bblockdiv', $(this));"><p>Delete account</p></div></td>
		</tr>
	</table>
</div>
<br/><br/>

<div class="box hide" id="changePass">
	<h2>Change Your Password</h2>
	<form action="" method="post" onsubmit="return (validatePassword() && checkPasswordsMatch());">
		Current password : <input id="opass" type="password" name="opass" onchange="old" placeholder="Old password" required /><br/>
		New password : <input id="pass" type="password" name="npass" onchange="validatePassword();" placeholder="New password"  required />
		<div id="passstrengthfb" class="fbmsg"></div>
		Confirm new password : <input id="cpass" type="password" name="cnpass" onchange="validatePassword(); checkPasswordsMatch();" placeholder="Confirm new password" required />
		<div id="passmatchfb" class="fbmsg"></div>
		<input type="submit" name="changepass" value="Change Password" />
	</form>
</div>

<div class="box hide" id="changeEmail">
  <h2>Change the email registered with this account</h2>
	<form action="" method="post" onsubmit="return checkEmail();">
		Current email : <input type="email" name="oldemail" value="<?php echo $_SESSION['email'];?>" disabled />
		New email : <input id="email" type="email" name="email" onchange="checkEmail();" placeholder="New email" value="<?php if (isset($_SESSION['postval']['email'])) echo $_SESSION['postval']['email'];?>" maxlength="254" required />
		<div id="emailfb" class="fbmsg"></div>
		<input type="submit" name="changeemail" value="Change Email" />
	</form>
</div>

<div class="box hide" id="changeUname">
  <h2>Change your username</h2>
	<form action="" action="" method="post" onsubmit="return checkUsername();" />
		Current username : <div style="position:absolute;margin:20px -15px;">@</div><input type="text" name="olduname" value="<?php echo substr($_SESSION['username'], 1);?>" disabled />
		New username : <div style="position:absolute;margin:20px -15px;">@</div><input id="uname" type="text" name="uname" onchange="checkUsername();" placeholder="New username" value="<?php if (isset($_SESSION['postval']['uname'])) echo $_SESSION['postval']['uname'];?>" maxlength="16" required />
		<div id="unamefb" class="fbmsg"></div>
		<input type="submit" name="changeuname" value="Change Username" />
	</form>
</div>

<div class="box hide" id="changeInfo">
  <h2>Change your informations</h2>
	<form action="" id="changeInfo" action="" method="post" onsubmit=""/>
		First name : <input type="text" name="fname" placeholder="First Name" value="<?php if (isset($_SESSION['postval']['fname'])) echo $_SESSION['postval']['fname']; else echo $_SESSION['firstname'];?>" maxlength="16" required />
		Last name : <input type="text" name="lname" placeholder="Last name" value="<?php if (isset($_SESSION['postval']['lname'])) echo $_SESSION['postval']['lname']; else echo $_SESSION['lastname'];?>" maxlength="16" required />
		Pseudo : <input type="text" name="pseudo" placeholder="Pseudo" value="<?php if (isset($_SESSION['postval']['pseudo'])) echo $_SESSION['postval']['pseudo']; else echo $_SESSION['pseudo'];?>" maxlength="16" required />
		<table class="formtable"><tr><th>Birthday&nbsp;:&nbsp;</th><td><select id="bday" name="bday"></select></td><td><select id="bmonth" name="bmonth" onchange="validDays('byear', 'bmonth', 'bday')"></select></td><td><select id="byear" name="byear" onchange="validDays('byear', 'bmonth', 'bday')"></select></td></tr></table>
		<table class="formtable">
			<tr>
				<th>Gender&nbsp;:&nbsp;</th>
				<td><input id="maleradio" type="radio" name="gender" value="male" checked /><label for="maleradio"> Male </label></td>
				<td><input id="femaleradio" type="radio" name="gender" value="female" <?php if (isset($_SESSION['postval']['gender'])) {if ($_SESSION['postval']['gender']=="female") echo 'checked';} else if ($_SESSION['gender']=="female") echo 'checked';?> /><label for="femaleradio"> Female </label></td>
				<td><input id="otherradio"type="radio" name="gender" value="other" <?php if (isset($_SESSION['postval']['gender'])) {if ($_SESSION['postval']['gender']=="other") echo 'checked';} else if ($_SESSION['gender']=="other") echo 'checked';?> /><label for="otherradio"> Other </label></td>
			</tr>
		</table>
  		<table class="formtable searchtable" style="border:none;">
			<tr style="border:none;">
  				<th style="border:none;">Spoken Languages : </th>
  				<td>
  					<dl id="spoklang" class="dropdown">
  						<input type='text' name='lang' class="formMultiSel" hidden />
  						<input type='text' class="multiSel readonly" readonly disabled />
  						<div class="nothSel">Nothing selected</div>
						<dt id="spoklangt" onclick="dropdown('spoklang');">
						  <div>Select</div>
						</dt>
						<dd>
							<div class="mutliSelect">
								<ul>
									<?php
                  $query = $conn->query("SELECT id,language FROM languages") or die($conn->error);
									while ($result = $query->fetch()) echo '<li><input id="l'.$result['id'].'" type="checkbox" value="'.$result['id'].'" />'.$result['language'].'</li>';
									?>
								</ul>
							</div>
						</dd>
  					</dl>
  				</td>
  			</tr>
  			<tr style="border:none;">
  				<th style="border:none;">Programming Languages : </th>
  				<td>
  					<dl id="proglang" class="dropdown">
  						<input type='text' name='proglang' class="formMultiSel" hidden />
  						<input type='text' class="multiSel readonly" readonly disabled />
  						<div class="nothSel">Nothing selected</div>
						<dt onclick="dropdown('proglang');">
						  <div>Select</div>
						</dt>
						<dd>
							<div class="mutliSelect">
								<ul>
									<?php
                  $query = $conn->query("SELECT id,prog_language FROM prog_languages");
									while ($result = $query->fetch()) echo '<li><input id="pl'.$result['id'].'" type="checkbox" value="'.$result['id'].'" />'.$result['prog_language'].'</li>';
									?>
								</ul>
							</div>
						</dd>
  					</dl>
  				</td>
  			</tr>
  			<tr style="border:none;">
  				<th style="border:none;">Programming Paradigms : </th>
  				<td>
  					<dl id="progpara" class="dropdown">
  						<input type='text' name='progpara' class="formMultiSel" hidden />
  						<input type='text' class="multiSel readonly" readonly disabled />
  						<div class="nothSel">Nothing selected</div>
						<dt onclick="dropdown('progpara');">
						  <div>Select</div>
						</dt>
						<dd>
							<div class="mutliSelect">
								<ul>
  									<?php
                    $query = $conn->query("SELECT id,prog_paradigm FROM prog_paradigms");
  									while ($result = $query->fetch()) echo '<li><input id="pp'.$result['id'].'" type="checkbox" value="'.$result['id'].'" />'.$result['prog_paradigm'].'</li>';
  									?>
  								</ul>
							</div>
						</dd>
  					</dl>
  				</td>
  			</tr>
  			<tr style="border:none;">
  				<th style="border:none;">Interests : </th>
  				<td>
  					<dl id="interests" class="dropdown">
  						<input type='text' name='interests' class="formMultiSel" hidden />
  						<input type='text' class="multiSel readonly" readonly disabled />
  						<div class="nothSel">Nothing selected</div>
						<dt onclick="dropdown('interests');">
						  <div>Select</div>
						</dt>
						<dd>
							<div class="mutliSelect">
								<ul>
  									<?php
                    $query = $conn->query("SELECT id,interest FROM interests");
  									while ($result = $query->fetch()) echo '<li><input id="i'.$result['id'].'" type="checkbox" value="'.$result['id'].'" />'.$result['interest'].'</li>';
  									?>
								</ul>
							</div>
						</dd>
  					</dl>
  				</td>
  			</tr>
  		</table>
		<input type="submit" name="changeinfos" value="Save changes"/>
	</form>
</div>

<div class="box hide" id="deleteAccount">
  <h2>Delete your account</h2>
	<form action="" id="changeInfo" action="" method="post" onsubmit="">
		<input id="pass" type="password" name="pass" placeholder="Password"  required />
		<p style="text-align:center; margin: 10px 0 -30px 0;"><input type="checkbox" name="deletewarning" value="warning"/>I am aware of the consequences of this action<p/>
		<p style="text-align:center; margin: 30px 0 -30px 0;"><input type="checkbox" name="deleteposts" value="warning"/>I am also willing to remove posts sent with this account<p/>
		<input type="submit" name="deleteaccount" value="Delete Account" />
	</form>
  <h3>Warning : this action cannot be undone!</h3>
</div>

<h3 class="errmsg fbmsg"><?php if (isset($_SESSION['fbmsg'])) echo $_SESSION['fbmsg']; ?></h3>

<?php
unset($_SESSION['fbmsg']);
?>

<?php include('footer.php'); ?>
