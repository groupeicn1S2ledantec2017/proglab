<?php include("header.php"); ?>

<div class="info">
<?php
if (isset($_SESSION['passupdated'])) {
  session_destroy();
  echo '<h1>Password updated!</h1><h2>You can now <a href="index.php#login">log in</a>!</h2></div>';
  include("footer.php");
  die();
}

if (isset($_GET['user']) && isset($_GET['code']) ) {
	$u = $_GET['user'];
	$code = strtolower($_GET['code']);

	$query = $conn->prepare("SELECT username,email,activated,blocked,shutdown FROM users WHERE id = ?");
	$query->execute([$u]);
	$user = $query->fetch();

	if ($user) {
		if (!$user['shutdown']) {
			if ($user['activated']) {
				$select_query = $conn->query("SELECT recovery_code FROM account_recovery WHERE username='$user[username]'");
				$select_result = $select_query->fetch();

				if (isset($select_result['recovery_code'])) {
					$_recoveryCode = new RecoveryCode($user['username'], $user['email']);

					if ($_recoveryCode->check($code)) {
						echo "<h1>Congratulations! Your account can now be recovered!</h1>";
						echo "<h2>Please enter your new password!</h2>";
            echo "<h3>Warning! For security reasons, if you refresh or leave this page before you updated your password, you will have to request for another account recovery email.</h3></div><div>";

            echo '
              <form id="reset" class="box container" method="post" action="reset_password.php" onsubmit="return (checkInputs(\'reset\') && validatePassword() && checkPasswordsMatch());">
                <h2>Reset Password</h2>
                <hr/>
                <input id="uname" type="hidden" name="uname" value="'.$user['username'].'" />
                <input id="pass" type="password" name="pass" onchange="validatePassword(); if($(\'#cpass\').val()!=\'\') checkPasswordsMatch();" placeholder="New Password" required />
            		<div id="passstrengthfb" class="fbmsg"></div>
                <input id="cpass" type="password" name="cpass" onchange="checkPasswordsMatch();" placeholder="Confirm New Password" required />
            		<div id="passmatchfb" class="fbmsg"></div>
            		<input type="submit" name="reset" value="Reset Password" required />
            	</form>';
					} else {
						echo "<h1>The code you provided is wrong! It has probably expired!</h1>";
						echo "<h2>You can be sent a new recovery email <a href='email_recoverycode.php'>here</a>!</h2>";
					}
				}	else {
					echo "<h1>Sorry, your recovery code wasn't found in the database!</h1>";
					echo "<h2>You can be sent a new recovery email <a href='email_recoverycode.php'>here</a>!</h2>";
				}
			} else {
        echo "The account registered with this email address is not activated!";
        echo "Didn't receive an activation email? <a href='email_activationcode.php'>Send again</a>!";
			}
		} else {
			echo "<h1>The account registered with this email address is shut down!</h1>";
			echo "<h2>We are sorry for that! You can <a href='index.php?a=signup'>create another account</a>!</h2>";
		}
	} else {
		echo "<h1>User does not exists! Please check your informations!</h1>";
		echo "<h2>Not registered yet? <a href='index.php?a=signup'>Create an account</a>!</h2>";
	}
} else {
	echo "<h1>Your url didn't provide the necessary tokens to recover the account!</h1>";
	echo "<h2>You can be sent a new activation email <a href='email_activationcode.php'>here</a>!</h2>";
}
?>
</div>

<?php include("footer.php"); ?>
