<?php include('main.php'); ?>

<?php
header('Content-Type: application/json');

$target_dir = "uploads/";

do $folder = random_hexstring(8); while (file_exists($target_dir.$folder));

$dir = $target_dir.$folder.'/';
mkdir($dir);

$uploaded = array();

if (!empty($_FILES['postfiles']['name'][0])) {
	foreach ($_FILES['postfiles']['name'] as $position => $name) if (move_uploaded_file($_FILES['postfiles']['tmp_name'][$position], $dir.$name)) $uploaded[] = array('name' => $name,'postfiles' => $dir.$name);

	$_POST['description'] = isset($_POST['description']) ? $_POST['description'] : '';

	$file = 1;
	$description = $_POST['description'];
	$date_added = date("Y-m-d H:i:s");
	$by = $_SESSION['username'];
	$to = $_POST['to'];
	$visibility = $_POST['visibility'];
	$likes = '';

	$query = $conn->prepare("INSERT INTO posts (file, body, date_added, posted_by, posted_to, visibility, likes) VALUES (?, ?, ?, ?, ?, ?, ?)");
	$query->execute([$file, $description, $date_added, $by, $to, $visibility, $likes]);

	$pid = $conn->lastInsertId();

	$query = $conn->prepare("INSERT INTO posts_documents (post_id, path) VALUES (?, ?)");
	$query->execute([$pid, $dir]);

	echo json_encode($uploaded);
}
?>
