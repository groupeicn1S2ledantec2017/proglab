<?php include('header.php'); ?>

<?php
if (isset($_SESSION["username"])) {
	header('Location: home.php');
	exit;
}
?>

<script type="text/javascript">
	$(document).ready(function() {
		<?php
		if (isset($_GET['a'])) {
			echo "$(\".pres\").css('opacity', '0.5');
			$(\".indexform\").css('opacity', '0.75');
			var pos = $('#".$_GET['a']."').offset().top - 100;
			$('html, body').animate({scrollTop:pos},500);
			focusonbox(\"".$_GET['a']."\");";
		}
		if (isset($_SESSION['postval']['bday'])) echo "$('#bday option[value=\"".$_SESSION['postval']['bday']."\"]').attr('selected',true);";
		if (isset($_SESSION['postval']['bmonth'])) echo "$('#bmonth option[value=\"".$_SESSION['postval']['bmonth']."\"]').attr('selected',true);";
		if (isset($_SESSION['postval']['byear'])) echo "$('#byear option[value=\"".$_SESSION['postval']['byear']."\"]').attr('selected',true);";
		?>
		validYears('byear');
		validMonth('bmonth');
		validDays('byear', 'bmonth', 'bday');
	});
</script>

<header>
	<h1>Welcome to ProgLab!</h1>
</header>

<div class="content">
	<div class="pres box">
		<h2>This social network is a wonderful way to find friends!</h2>
		<br/>
		<h2>You can create an account to discover it!</h2>
		<br/><br/>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sit amet augue molestie, fringilla massa in, imperdiet quam. Ut lobortis tellus et tortor faucibus porttitor. Vestibulum a faucibus urna. Donec et dictum est. Etiam sed arcu in ligula sollicitudin eleifend non maximus felis. Donec ornare pulvinar libero at mollis. Donec odio mauris, euismod sed vulputate sed, pretium at ex. Aenean eget gravida nisi. Ut ut sagittis quam, sed accumsan ex.</p>
		<p>Nulla vel rutrum velit. Fusce tristique semper dui in posuere. Etiam porta dictum lorem, ut eleifend justo mollis ac. Maecenas quam ex, dictum eget faucibus et, pharetra a dolor. Suspendisse a turpis sollicitudin, pretium mi sit amet, elementum massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis vel gravida est. In elit turpis, bibendum non est dapibus, fringilla sagittis est. Ut quam diam, pretium ut nisi et, efficitur mattis purus. Donec ullamcorper pulvinar interdum. Suspendisse consequat felis rhoncus mauris tincidunt, ut facilisis tellus aliquam. Quisque tempus dignissim finibus.</p>
		<br/>
		<i><b>Projet réalisé par : Thomas GHOBRIL, Guirec LE PIVERT, Corentin BOUNON et Gwendal MOUDEN. <br/>Le code peut être réutilisé, modifié, et distribué librement.</b></i>
	</div>

	<form id="login" class="box indexform" method="post" action="login.php" onsubmit="return (checkInputs('login'));">
		<div class="scsmsg fbmsg"><?php if (isset($_SESSION['fbmsg']['signupfb'])) echo $_SESSION['fbmsg']['signupfb']; ?></div>
		<h2>Log In</h2>
		<hr/>
		<input type="text" name="identifier" placeholder="Email or Username" value="<?php if (isset($_SESSION['postval']['identifier'])) echo $_SESSION['postval']['identifier']; ?>" required />

		<input type="password" name="pass" placeholder="Password" required />

		<input type="submit" name="login" value="Log in" />

		<div id="loginerr" class="errmsg fbmsg"><?php if (isset($_SESSION['fbmsg']['loginerrmsg'])) echo $_SESSION['fbmsg']['loginerrmsg']; ?></div>

		<div id="loginsol" class="fbmsg"><?php if (isset($_SESSION['fbmsg']['loginsolmsg'])) echo $_SESSION['fbmsg']['loginsolmsg']; ?></div>
	</form>

	<form id="signup" class="box indexform" method="post" action="signup.php" onsubmit="return (checkInputs('signup') && checkUsername() && checkEmail() && validatePassword() && checkPasswordsMatch());">
		<h2>Sign Up</h2>
		<hr/>
		<input type="text" name="fname" placeholder="First Name" value="<?php if (isset($_SESSION['postval']['fname'])) echo $_SESSION['postval']['fname'];?>" maxlength="16" required />

		<input type="text" name="lname" placeholder="Last name" value="<?php if (isset($_SESSION['postval']['lname'])) echo $_SESSION['postval']['lname'];?>" maxlength="16" required />

		<input type="text" name="pseudo" placeholder="Pseudo" value="<?php if (isset($_SESSION['postval']['pseudo'])) echo $_SESSION['postval']['pseudo'];?>" maxlength="16" required />

		<div style="position:absolute;margin:20px -15px;">@</div><input id="uname" type="text" name="uname" onchange="checkUsername();" placeholder="Username" value="<?php if (isset($_SESSION['postval']['uname'])) /*if (!$_SESSION['fbmsg']['un_used'])*/ echo $_SESSION['postval']['uname'];?>" maxlength="16" required />

		<div id="unamefb" class="fbmsg"></div>

		<input id="email" type="email" name="email" onchange="checkEmail();" placeholder="Email" value="<?php if (isset($_SESSION['postval']['email'])) /*if (!$_SESSION['fbmsg']['em_used'] && !$_SESSION['fbmsg']['em_invalid'])*/ echo $_SESSION['postval']['email'];?>" maxlength="254" required />

		<div id="emailfb" class="fbmsg"></div>

		<table class="formtable"><tr><th>Birthday&nbsp;:&nbsp;</th><td><select id="bday" name="bday"></select></td><td><select id="bmonth" name="bmonth" onchange="validDays('byear', 'bmonth', 'bday')"></select></td><td><select id="byear" name="byear" onchange="validDays('byear', 'bmonth', 'bday')"></select></td></tr></table>

		<table class="formtable">
			<tr>
				<th>Gender&nbsp;:&nbsp;</th>
				<td><input id="maleradio" type="radio" name="gender" value="male" checked /><label for="maleradio"> Male </label></td>
				<td><input id="femaleradio" type="radio" name="gender" value="female" <?php if (isset($_SESSION['postval']['gender'])) if ($_SESSION['postval']['gender']=="female") echo 'checked';?> /><label for="femaleradio"> Female </label></td>
				<td><input id="otherradio"type="radio" name="gender" value="other" <?php if (isset($_SESSION['postval']['gender'])) if ($_SESSION['postval']['gender']=="other") echo 'checked';?> /><label for="otherradio"> Other </label></td>
			</tr>
		</table>

		<input id="pass" type="password" name="pass" onchange="validatePassword(); if($('#cpass').val()!='') checkPasswordsMatch();" placeholder="Password" required />

		<div id="passstrengthfb" class="fbmsg"></div>

		<input id="cpass" type="password" name="cpass" onchange="checkPasswordsMatch();" placeholder="Confirm Password" required />

		<div id="passmatchfb" class="fbmsg"></div>

		<input type="submit" name="signup" value="Sign Up" required />

		<div id="signuperr" class="errmsg fbmsg"><?php if (isset($_SESSION['fbmsg']['signupmsg'])) echo $_SESSION['fbmsg']['signupmsg']; ?></div>
	</form>
</div>

<?php
unset($_SESSION['postval']);
unset($_SESSION['fbmsg']);
?>



<?php include('footer.php'); ?>
