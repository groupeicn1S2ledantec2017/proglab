<div class="box boxform">
	<h2>Post something on <?php echo ((isset($_SESSION['profile']['username']) && $_SESSION['profile']['username']!=$_SESSION['username']) ? $_SESSION['profile']['firstname']."'s" : "your");?> wall</h2>
	<hr/>
	<table style="margin:0 auto;padding:0;">
		<tr style="margin:0 auto;padding:0;">
			<td style="margin:0 auto;padding:0;width:50%;">
				<div id='textmodebutton' class="bblockdiv dsub" name="text" onclick="changePostMode('filemodebutton', 'textmodebutton', 'filemode', 'textmode');">Text</div>
			</td>
			<td style="margin:0 auto;padding:0;width:50%;">
				<div id='filemodebutton' class="bblockdiv subh" name="file" onclick="changePostMode('textmodebutton', 'filemodebutton', 'textmode', 'filemode');">File</div>
			</td>
		</tr>
	</table>

	<div id="textmode">
		<form id="textpostform" action="send_post.php" method="post" onsubmit="send_post($('#post_body').val(), $('input[name=\'visibility\']:checked').val(), '<?php echo (isset($_SESSION['profile']['username']) ? $_SESSION['profile']['username'] : $_SESSION["username"]);?>', 'posttextfb'); $('#post_body').val(''); setTimeout(function() {$('#posts').load(document.URL+' #posts')},100); return false;">
			<table class="formtable"><tr><th>Visibility&nbsp;:&nbsp;</th><td><input type="radio" name="visibility" value="public" checked /> Public </td><td><input type="radio" name="visibility" value="ffriends" /> Friends of friends </td><td><input type="radio" name="visibility" value="friends" /> Friends</td></tr></table>

			<textarea id="post_body" name="post_body" placeholder="Write something..."></textarea>

			<input type="submit" name="send" value="Post" />

			<div id="posttextfb" class="fbmsg"></div>
		</form>
	</div>

	<div id="filemode" style="display:none;">
		<form id="filepostform" action="send_postfile.php" method="post" enctype="multipart/form-data" onsubmit="send_postfile('fileinput', $('#fileinputdescription').val(), $('input[name=\'visibility\']:checked').val(), '<?php echo (isset($_SESSION['profile']['username']) ? $_SESSION['profile']['username'] : $_SESSION["username"]);?>', 'prog', 'fileinputfb', 'abort', 'postfilefb'); $('#fileinput').val(''); $('#fileinputdescription').val(''); fileInputText('fileinput', 'fileinputfb'); setTimeout(function() {$('#posts').load(document.URL+' #posts')},100); return false;">
			<table class="formtable"><tr><th>Visibility&nbsp;:&nbsp;</th><td><input type="radio" name="visibility" value="public" /> Public </td><td><input type="radio" name="visibility" value="ffriends" /> Friends of friends </td><td><input type="radio" name="visibility" value="friends" /> Friends</td></tr></table>

			<input id="fileinputdescription" type="text" name="file_description" placeholder="Description..." />

			<input id="fileinput" class="fileinput" onchange="fileInputText('fileinput', 'fileinputfb');" type="file" name="postfiles[]" multiple />
			<label id="fileinputlabel" class="fileinputbutton" for="fileinput"><strong>Choose one or multiple files </strong><span class="dragndrop">or drag and drop them here </span></label>

			<div id="fileinputfb" class="bblockdiv dsub">No file chosen</div>

			<progress id="prog" value="0" min="0" max="100" style="display:none;"></progress>
			<div role="button" class="blockdiv csubh" id="abort" style="display:none;">Abort</div>

			<input type="submit" name="send" value="Post" />

			<div id="postfilefb" class="fbmsg"></div>
		</form>
		<div id='xhr2notsupportedfb' style='display:none;'><div  class="errmsg fbmsg"><h3>File upload unavailable!</h3><h3 class="errmsg">Your browser doesn't support AJAX file upload (XHR2)!</h3></div><p style="text-align:center;">Please download the lastest version of a reliable internet browser (such as Firefox or Chrome) and try again.</p></div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		if (supportAdvancedUpload('filepostform', 'xhr2notsupportedfb')) {
			fileInputDragndrop('fileinput','fileinputlabel');
		}
	});
</script>
