Projet réalisé par : Thomas GHOBRIL, Guirec LE PIVERT, Corentin BOUNON et Gwendal MOUDEN.

Ce projet a été réalisé dans le cadre d'un projet de fin d'année pour l'option informatique au lycée.

Le code peut être réutilisé, modifié, et distribué librement.


Languages et Versions :
PHP 7.2,
MySQL 5.7,
JavaScript (dont JQuery 3.1.1),
HTML 5,
CSS 3.