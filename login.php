<?php include('header.php'); ?>

<?php
$_SESSION['fbmsg']['loginerrmsg']='';
$_SESSION['fbmsg']['loginsolmsg']='';
if (isset($_POST['login'])) {
	$identifier = htmlentities(strtolower($_POST['identifier']));

	$idu = ($identifier[0]=='@') ? $identifier : '@'.$identifier;
	$ide = $identifier;

	$pass = recursive_hash($_POST['pass']); // Slow recursive hashing process to have a secure hash for the password.
	unset($_POST['pass']); // Unset the password variable.

	$query = $conn->prepare("SELECT * FROM users WHERE username = ? OR email = ?");
 	$query->execute([$idu, $ide]);
 	$user = $query->fetch();

	if ($user) {
		if ($user['activated']) {
			if (!$user['shutdown']) {
				if (password_verify($pass, $user["password"])) {
					$_SESSION['username']=$user["username"];
					$_SESSION['firstname']=$user["firstname"];
					$_SESSION['lastname']=$user["lastname"];
					$_SESSION['pseudo']=$user["pseudo"];
					$_SESSION['email']=$user["email"];
					$_SESSION['biography']=$user["biography"];
					$_SESSION['gender']=$user["gender"];
					$_SESSION['birthday']=$user["birthday"];
					$_SESSION['last_activity']=$user["last_activity"];
					$_SESSION['friends']=$user["friends"];
					$_SESSION['ffriends']=$user["ffriends"];
					$_SESSION['profile_pic']=$user["profile_pic"];
					$_SESSION['followings']=$user["followings"];
					$_SESSION['followers']=$user["followers"];
					$_SESSION['languages']=$user["languages"];
					$_SESSION['prog_languages']=$user["prog_languages"];
					$_SESSION['prog_paradigms']=$user["prog_paradigms"];
					$_SESSION['interests']=$user["interests"];
					$_SESSION["password"]=$pass;

					unset($_POST);
					unset($_SESSION['fbmsg']);

					header('Location: home.php');
					exit;
				} else {
					$_SESSION['fbmsg']['loginerrmsg']="Wrong password! Please check your informations!";
					$_SESSION['fbmsg']['loginsolmsg']="Forgotten password? <a href='email_recoverycode.php'>Send an account recovery email</a>!";
				}
			} else {
				$_SESSION['fbmsg']['loginerrmsg']="Account shut down!";
				$_SESSION['fbmsg']['loginsolmsg']="We are sorry for that! You can <a href='index.php?a=signup'>create another account</a>!";
			}
		} else {
			$_SESSION['fbmsg']['loginerrmsg']="Account unactivated! Please check your emails to activate your account!";
			$_SESSION['fbmsg']['loginsolmsg']="Didn't receive an activation email? <a href='email_activationcode.php'>Send again</a>!";
		}
	} else {
		$_SESSION['fbmsg']['loginerrmsg']="User does not exist! Please check your informations!";
		$_SESSION['fbmsg']['loginsolmsg']="Not registered yet? <a href='index.php?a=signup'>Create an account</a>!";
	}

	$_SESSION['postval']=$_POST;
	header("Location: index.php?a=login");
	exit;
}
?>

<?php include('footer.php'); ?>
