<?php include('connection.php'); ?>

<?php header("Cache-Control: no-cache, no-store, must-revalidate"); ?>

<?php
class PostForm {
	function __construct() {
    if (isset($_SESSION["username"])) {
			include('postform.php');
		}
  }
}

class PostItem {
	public $id, $pdata, $cdata, $ddata;

	function __construct($postid) {
		global $conn;
		$this->id = $postid;
		$pquery = $conn->query("SELECT * FROM posts WHERE id='$postid'");
		$pdata = $pquery->fetch();
		$this->pdata = array();
		$this->pdata = $pdata;
		$cquery = $conn->query("SELECT * FROM posts_comments WHERE post_id='$postid'");
		$this->cdata = array();
		while ($cdata = $cquery->fetch()) array_push($this->cdata, $cdata);
		if ($this->pdata['file']==1) {
			$fquery = $conn->query("SELECT * FROM posts_documents WHERE post_id='$postid'");
			$ddata = $fquery->fetch();
			$this->ddata = array();
			$this->ddata = $ddata;
		}
	}

	function display() {
		$_SESSION['temp']['post']['id'] = $this->id;
		$_SESSION['temp']['post']['body'] = $this->pdata['body'];
		if ($this->pdata['file']) $_SESSION['temp']['post']['path'] = $this->ddata['path'];
		$_SESSION['temp']['post']['date_added'] = $this->pdata['date_added'];
		$_SESSION['temp']['post']['posted_by'] = $this->pdata['posted_by'];
		$_SESSION['temp']['post']['posted_to'] = $this->pdata['posted_to'];
		$_SESSION['temp']['post']['visibility'] = $this->pdata['visibility'];
		$_SESSION['temp']['post']['likes'] = $this->pdata['likes'];
		$_SESSION['temp']['post_comments'] = array();
		foreach ($this->cdata as $i => $cd) {
			$_SESSION['temp']['post_comments'][$i]['id'] = $cd['id'];
			$_SESSION['temp']['post_comments'][$i]['body'] = $cd['body'];
			$_SESSION['temp']['post_comments'][$i]['date_added'] = $cd['date_added'];
			$_SESSION['temp']['post_comments'][$i]['posted_by'] = $cd['posted_by'];
			$_SESSION['temp']['post_comments'][$i]['likes'] = $cd['likes'];
		}
		include('postitem.php');
		unset($_SESSION['temp']);
	}

	function like() {
		global $conn;
		if (isset($_SESSION['username'])) {
			if (strpos($this->pdata['likes'],$_SESSION['username'].",")!==false) {
				$nlikes = str_replace($_SESSION['username'].",","",$this->pdata['likes']);
				$conn->query("UPDATE posts SET likes='$nlikes' WHERE id='$this->id'");
			} else {
				$nlikes = $this->pdata['likes'].$_SESSION['username'].",";
				$conn->query("UPDATE posts SET likes='$nlikes' WHERE id='$this->id'");
			}
		}
	}

	function likecomment($cid) {
		global $conn;
		if (isset($_SESSION['username'])) {
			foreach ($this->cdata as $cd) if ($cd['id'] == $cid) $comment=$cd;
			if (strpos($comment['likes'],$_SESSION['username'].",")!==false) {
				$nlikes = str_replace($_SESSION['username'].",","",$comment['likes']);
				$conn->query("UPDATE posts_comments SET likes='$nlikes' WHERE id='$comment[id]'");
			} else {
				$nlikes = $comment['likes'].$_SESSION['username'].",";
				$conn->query("UPDATE posts_comments SET likes='$nlikes' WHERE id='$comment[id]'");
			}
		}
	}

	function comment($body) {
		global $conn;
		if (isset($_SESSION['username'])) {
			$date = date("Y-m-d H:i:s");
			$likes='';
			$query = $conn->prepare("INSERT INTO posts_comments (post_id,body,date_added,posted_by,likes) VALUES (?, ?, ?, ?, ?)");
	 		$query->execute([$this->id, $body, $date, $_SESSION['username'], $likes]);
		}
	}
}

class FriendRequestForm {
	function __construct() {
		global $conn;

		if (isset($_SESSION["username"]) && isset($_SESSION['profile']['username'])) {
			include('friendrequestform.php');
		}
	}
}

class FollowForm {
	function __construct() {
		global $conn;

		if (isset($_SESSION["username"]) && isset($_SESSION['profile']['username'])) {
			include('followform.php');
		}
	}
}

abstract class SecretCode {
	public $user, $email, $table, $column, $action, $page, $subject;

	function send($code) {
		global $conn;

		$codehash = password_hash($code, PASSWORD_DEFAULT);
		$delete_query = $conn->query("DELETE FROM {$this->table} WHERE username='$this->user'");
		$insert_query = $conn->query("INSERT INTO {$this->table} (username, {$this->column}) VALUES ('$this->user', '$codehash')");

		$link = $this->page."?user=".$this->user."&code=".$code;

		$to = $this->email;
		$subject = $this->subject;
		$message = '<html><head><style>body {text-align: center;font-family: Arial;}h1 {font-size:32px;}h2 {font-size: 24px;}</style></head><body><h1>ProgLab '.$this->subject.'</h1><h2><a href="'.$link.'">Follow this link to '.$this->action.'!</a></h2></body></html>';
		$headers = 'From: ProgLab Webmaster <webmaster@proglab.com>'."\r\n".'Reply-To: reply@proglab.com' ."\r\n".'X-Mailer: PHP/'.phpversion();
		$headers .= 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers.= "X-Priority: 1\r\n";

		$status = mail($to, $subject, $message, $headers);

		return $status;
	}

	function check($code) {
		global $conn;

		$select_query = $conn->query("SELECT {$this->column} FROM {$this->table} WHERE username='$this->user'");
		$select_result = $select_query->fetch();

		if (password_verify($code, $select_result[$this->column])) {
			$delete_query = $conn->query("DELETE FROM {$this->table} WHERE username='$this->user'");
			return true;
		} else {
			return false;
		}
	}
}
class ActivationCode extends SecretCode {
	public $user, $email, $table, $column, $action, $page, $subject;

	function __construct($u, $e) {
		$this->user = $u;
		$this->email = $e;
		$this->table = "account_activation";
		$this->column = "activation_code";
		$this->subject = "Account activation code";
		$this->action = "activate your account";
		$this->page = "http://localhost/proglab/check_activationcode.php";
	}
}
class RecoveryCode extends SecretCode {
	public $user, $email, $table, $column, $action, $page, $subject;

	function __construct($u, $e) {
		$this->user = $u;
		$this->email = $e;
		$this->table = "account_recovery";
		$this->column = "recovery_code";
		$this->subject = "Account recovery code";
		$this->action = "reset your password";
		$this->page = "http://localhost/proglab/check_recoverycode.php";
	}
}
?>

<?php
function recursive_hash($pass, $i=10000, $algos=array("sha512", "whirlpool")) {
	ini_set('max_execution_time', 0); // No time limit
	while ($i-- > 0) $pass = hash($algos[$i % count($algos)], $pass.hash($algos[($i+1) % count($algos)], $pass));
	return $pass;
}

function random_hexstring($length) {
	if ((int)phpversion()[0] < 7) return bin2hex(openssl_random_pseudo_bytes($length));
	else return bin2hex(random_bytes($length));
}

function str_format($str, $char, $formatting, $urlencodefirst=0) {
	$finalstr = '';
	foreach (preg_split("/\n/", $str) as $line) {
		foreach (explode(" ", $line) as $word) {
			if (!empty($word) && $word[0]==$char[0] && strlen($word) > 1 && preg_match('/^[A-Za-z0-9_]+$/',substr($word,1))) {

				$wordarr = preg_split("/\b\b/", $word);
				$tempstr = '';
				foreach ($formatting as $id => $part) {
					if ($id<$urlencodefirst) {
						$wordarr[0] = urlencode($wordarr[0]);
					} else {
						$wordarr[0] = urldecode($wordarr[0]);
					}
					$tempstr .= $part.$wordarr[0].$wordarr[1];
				}
				$tempstr = substr($tempstr,0,strlen($tempstr)-strlen($wordarr[0].$wordarr[1]));
				$finalstr .= $tempstr;
				foreach ($wordarr as $index=>$wordpart) if ($index>1) $finalstr .= $wordpart;
				$finalstr .= ' ';
			} else {
				$finalstr .= $word." ";
			}
		}
		$finalstr = substr($finalstr,0,strlen($finalstr)-1)."\n";
	}
	return substr($finalstr,0,strlen($finalstr)-1);
}

set_error_handler('exceptions_error_handler');
function exceptions_error_handler($severity, $message, $filename, $lineno) {
  if (error_reporting() == 0) {
    return;
  }
  if (error_reporting() & $severity) {
    throw new ErrorException($message, 0, $severity, $filename, $lineno);
  }
}
?>
