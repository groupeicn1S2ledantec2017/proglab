<?php include("header.php") ?>

<?php
header('Content-Type: text/html; charset=utf-8');
?>

<form id="sendactcode" class="box" method="post" action="send_activationcode.php" onsubmit="return (checkInputs('login') && checkEmail());">
	<h2>Send account activation code</h2>
	<hr/>
	<input id="email" type="email" name="email" onchange="checkEmail();" placeholder="Email Address" value="" maxlength="254" required />
	
	<div id="emailfb" class="fbmsg"></div>
	
	<input type="submit" name="sendemail" value="Send email" />
	
	<div id="scs" class="scsmsg fbmsg"><?php if (isset($_SESSION['fbmsg']['emcheckscsmsg'])) if ($_SESSION['fbmsg']['emcheckscsmsg']!="") echo '<br/>'.$_SESSION['fbmsg']['emcheckscsmsg']; ?></div>
	
	<div id="err" class="errmsg fbmsg"><?php if (isset($_SESSION['fbmsg']['emcheckerrmsg'])) if ($_SESSION['fbmsg']['emcheckerrmsg']!="") echo $_SESSION['fbmsg']['emcheckerrmsg']; ?></div>
	
	<div id="loginsol" class="fbmsg"><?php if (isset($_SESSION['fbmsg']['emchecksolmsg'])) if ($_SESSION['fbmsg']['emchecksolmsg']!="") echo $_SESSION['fbmsg']['emchecksolmsg']; ?></div>
</form>

<?php
unset($_SESSION['fbmsg']);
?>

<?php include("footer.php") ?>
