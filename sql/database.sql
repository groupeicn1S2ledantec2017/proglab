CREATE DATABASE IF NOT EXISTS proglab CHARACTER SET latin1;

USE proglab;

CREATE TABLE IF NOT EXISTS users (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	username VARCHAR(255) NOT NULL,
	firstname VARCHAR(255) NOT NULL,
	lastname VARCHAR(255) NOT NULL,
	pseudo VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	password TEXT NOT NULL,
	biography TEXT,
	gender ENUM('male', 'female', 'other') NOT NULL,
	last_activity DATETIME NOT NULL,
	birthday DATE NOT NULL,
	profile_pic TEXT,
	friends TEXT,
	ffriends TEXT,
	followings TEXT,
	followers TEXT,
	blocked TEXT,
	languages TEXT,
	prog_languages TEXT,
	prog_paradigms TEXT,
	interests TEXT,
	activated BOOLEAN NOT NULL,
	shutdown BOOLEAN NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS account_activation (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	username VARCHAR(255) NOT NULL,
	activation_code VARCHAR(255) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS account_recovery (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	username VARCHAR(255) NOT NULL,
	recovery_code VARCHAR(255) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS friend_requests (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	sent_by VARCHAR(255) NOT NULL,
	sent_to VARCHAR(255) NOT NULL,
	rejected BOOLEAN NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS messages (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	body TEXT NOT NULL,
	date_sent DATETIME NOT NULL,
	posted_by VARCHAR(255) NOT NULL,
	chat_id INT(11) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS messages_documents (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	path TEXT NOT NULL,
	description TEXT NOT NULL,
	date_sent DATETIME NOT NULL,
	posted_by VARCHAR(255) NOT NULL,
	chat_id INT(11) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS chats (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	members TEXT NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS posts (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  file BOOLEAN NOT NULL,
  body TEXT CHARACTER SET latin1 COLLATE latin1_fulltext_ci NOT NULL,
  date_added DATETIME NOT NULL,
  posted_by VARCHAR(255) NOT NULL,
  posted_to VARCHAR(255) NOT NULL,
  visibility ENUM('friends', 'ffriends', 'public') NOT NULL,
  likes TEXT NOT NULL,
  PRIMARY KEY (id),
  FULLTEXT INDEX idx(body)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS posts_comments (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	post_id INT(11) UNSIGNED NOT NULL,
	body TEXT NOT NULL,
	date_added DATETIME NOT NULL,
	posted_by VARCHAR(255) NOT NULL,
	likes TEXT NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS posts_documents (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  post_id INT(11) UNSIGNED NOT NULL,
  path TEXT NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS interests (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	interest TEXT NOT NULL,
	PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS prog_languages (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	prog_language TEXT NOT NULL,
	PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS prog_paradigms (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	prog_paradigm TEXT NOT NULL,
	PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS languages (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	language TEXT NOT NULL,
	code TEXT NOT NULL,
	PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
